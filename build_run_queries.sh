#!/bin/bash
sudo echo "Triggered TFA."

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
FILES="$DIR/"$1"/queries/*.txt"

echo "Building queries."
php "$DIR"/qbuild.php $1

for f in $FILES
do
  echo "Executing $f."
  `sudo drush --root=/var/www/staging.devdrupal.org/htdocs/ sql-connect` < "$f" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > "$f".out.csv
done
mv "$DIR"/"$1"/queries/*.csv "$DIR"/"$1"/csv/

