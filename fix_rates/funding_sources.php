<?php
include_once(__DIR__ . '/../src/ResultSet.php');

use Drupal\core_metrics\ResultSet;

$r = new ResultSet('fix_rates');
$criticals = $r->getColumn('80x_criticals', 'nid');
$file = file(__DIR__ . '/2015q1_commit_mentions.txt');
$d8_accelerate = file(__DIR__ . '/d8_accelerate.csv');
$data = json_decode($file[0]);
unset($d8_accelerate[0]);

$accelerated = array();
// Merge together all D8 accelerate grantees. Note that one issue might be in
// multiple roles.
foreach ($d8_accelerate as $row) {
  $fields = explode(',', $row);
  if (empty($accelerated[$fields[0]])) {
    $accelerated[$fields[0]] = array();
  }
  // Grantee 1.
  if (!empty($fields[2])) {
    $accelerated[$fields[0]][] = strtolower($fields[2]);
  }
  // Grantee 2.
  if (!empty($fields[3])) {
    $accelerated[$fields[0]][] = strtolower($fields[3]);
  }
}

$mentions = array();
$mentions['octo'] = 0;
$mentions['non_octo'] = 0;
$mentions['other'] = 0;
$mentions['d8_accelerate'] = 0;

$critical_mentions = array();
$critical_mentions['octo'] = 0;
$critical_mentions['non_octo'] = 0;
$critical_mentions['other'] = 0;
$critical_mentions['d8_accelerate'] = 0;

foreach ($data as $commit) {
  $is_critical = in_array($commit->nid, $criticals);
  foreach ($commit->contributors as $contributor) {
    if (in_array($contributor, array('xjm', 'tim.plunkett', 'gabor_hojtsy', 'wim leers', 'effulgentsia'))) {
      $mentions['octo']++;
      if ($is_critical) {
        $critical_mentions['octo']++;
      }
    }
    elseif (in_array($contributor, array('alexpott', 'yesct', 'greg.1.anderson'))) {
      $mentions['non_octo']++;
      if ($is_critical) {
        $critical_mentions['non_octo']++;
      }
    }
    elseif (!empty($accelerated[$commit->nid]) && in_array($contributor, $accelerated[$commit->nid])) {
      $mentions['d8_accelerate']++;
      if ($is_critical) {
        $critical_mentions['d8_accelerate']++;
      }
    }
    else {
      $mentions['other']++;
      if ($is_critical) {
        $critical_mentions['other']++;
      }

    }
  }
}

print_r($mentions);
print_r($critical_mentions);
