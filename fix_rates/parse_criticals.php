<?php
include_once(__DIR__ . '/../src/ResultSet.php');

use Drupal\core_metrics\ResultSet;

$r = new ResultSet('fix_rates');
$issues = array();
$criticals = array();
$criticals['80x'] = $r->getColumn('80x_criticals', 'nid');
$criticals['7x'] = $r->getColumn('7x_criticals', 'nid');
$issues['80x'] = $r->getResult('80x_all');
$issues['7x'] = $r->getResult('7x_all');
$issues['80x_promoted'] = $r->getResult('80x_criticals_promoted');
$issues['7x_promoted'] = $r->getResult('7x_criticals_promoted');

$commits = array();
$file_80x = file(__DIR__ . '/80x_commits.txt');
$commits['80x'] = json_decode($file_80x[0]);
$file_7x = file(__DIR__ . '/7x_commits.txt');
$commits['7x'] = json_decode($file_7x[0]);

$issue_dates = array();
$aggregated = array();
foreach (array('80x', '7x') as $v) {
  foreach ($issues[$v] as $node) {
    $issue_dates[$v][$node[0]] = array(
      'created' => $node[1],
      'fixed' => $node[2],
    );
  }

  foreach ($issues[$v . '_promoted'] as $node) {
    $issue_dates[$v][$node[0]]['promoted'][] = $node[3];
  }

  foreach ($commits[$v] as $commit) {
    // Skip commits with no node ID and non-criticals.
    if (empty($commit->nid) || empty($issue_dates[$v][$commit->nid])) {
      continue;
    }

    $is_critical = in_array($commit->nid, $criticals[$v]);
    // Use the created and promoted dates from the result set.
    $aggregated['all'][$v][$commit->nid]['created'] = $issue_dates[$v][$commit->nid]['created'];

    // Use the latest commit date for the fix date.
    if (empty($aggregated['all'][$v][$commit->nid]['fixed']) || strtotime($aggregated['all'][$v][$commit->nid]['fixed']) < strtotime($commit->date)) {
      $aggregated['all'][$v][$commit->nid]['fixed'] = $commit->date;
    }

    if ($is_critical) {
      // Set the promoted date.
      $aggregated['all'][$v][$commit->nid]['promoted'] = '';
      if (!empty($issue_dates[$v][$commit->nid]['promoted'])) {
        // Promoted dates are in an array. Pick the most recent one before the
        // fix date.
        foreach ($issue_dates[$v][$commit->nid]['promoted'] as $promoted) {
          if (strtotime($promoted) < strtotime($commit->date)) {
            if (empty($aggregated['all'][$v][$commit->nid]['promoted']) || (strtotime($aggregated['all'][$v][$commit->nid]['promoted']) < strtotime($promoted))) {
              $aggregated['all'][$v][$commit->nid]['promoted'] = $promoted;
            }
          }
        }
      }

      // Store it to the separate critical list.
      $aggregated['critical'][$v][$commit->nid] = $aggregated['all'][$v][$commit->nid];
    }
  }

  foreach (array('all', 'critical') as $priority) {
    if ($priority == 'critical') {
      $date_types = array('created', 'promoted', 'fixed');
    }
    else {
      $date_types = array('created', 'fixed');
    }
    $aggregated_csv = 'nid,' . implode(',', $date_types) . "\n";
    foreach ($aggregated[$priority][$v] as $nid => $dates) {
      $aggregated_csv .= $nid;
      foreach ($date_types as $date) {
        $aggregated_csv .= ',' . $dates[$date];
      }
      $aggregated_csv .= "\n";
    }
    file_put_contents('./aggregated_' . $v . '_' . $priority . '_dates.csv', $aggregated_csv);
  }
}
