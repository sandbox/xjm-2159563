<?php

include_once(__DIR__ . '/../src/ResultSet.php');

use Drupal\core_metrics\ResultSet;

$r = new ResultSet('octo_2014');

$total_fixed_count = $r->getFirstCell('fixed_criticals');
$octo_fixed = array_unique(array_merge(
  $r->getColumn('octo_criticals', 'nid'),
  $r->getColumn('jesse_criticals', 'nid'),
  $r->getColumn('tim_criticals', 'nid'),
  $r->getColumn('devin_criticals', 'nid')
));
sort($octo_fixed);
print_r($octo_fixed);
print "\n\n OCTO helped fix " . sizeof($octo_fixed) . " of $total_fixed_count criticals\n\n";