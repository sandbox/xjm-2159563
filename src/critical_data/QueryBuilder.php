<?php

namespace Drupal\core_metrics\critical_data;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {

    $this->queries['critical_data'] = new IssueQuery();
    $this->queries['critical_data']
      ->selectDistinctNID()
      ->selectTitle()
      ->selectStatus()
      ->selectComponent()
      ->selectOwner()
      ->selectAge()
      ->selectUpdated()
      ->selectPatchUpdated()
      ->selectCommentCount()
      ->selectCommenters()
      ->selectTriagedTags()
      ->addOpen()
      ->addCritical()
      ->orderBy(array('last_patch_date', 'n.nid'))
      ;
  }

}
