<?php

namespace Drupal\core_metrics\d8up_data;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {
    $this->queries['d8up_data'] = new IssueQuery();
    $this->queries['d8up_data']
      ->selectDistinctNID()
      ->selectTitle()
      ->selectStatus()
      ->selectComponent()
      ->selectOwner()
      ->selectAge()
      ->selectUpdated()
      ->selectPatchUpdated()
      ->selectCommentCount()
      ->selectCommenters()
      ->addOpen()
      ->addCritical()
      ->addD8up()
      ;
  }

}
