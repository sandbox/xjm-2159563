<?php

namespace Drupal\core_metrics\fix_rates;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {

    $this->queries['80x_criticals'] = new IssueQuery();
    $this->queries['80x_criticals']
      ->selectDistinctNID()
      ->selectCreatedDate()
      ->selectFixedDate()
      ->addFixed()
      ->addCritical()
      ->fixedAfter('2011-03-09')
      ->orderBy(array('fixed_date'))
    ;

    $this->queries['80x_criticals_promoted'] = new IssueQuery();
    $this->queries['80x_criticals_promoted']
      ->selectDistinctNID()
      ->selectCreatedDate()
      ->selectFixedDate()
      ->selectChangedDate()
      ->addFixed()
      ->addCritical()
      ->newPriority(array(IssueQuery::CRITICAL))
      ->fixedAfter('2011-03-09')
      ->orderBy(array('fixed_date'))
    ;

    $this->queries['80x_open_criticals'] = new IssueQuery();
    $this->queries['80x_open_criticals']
      ->selectDistinctNID()
      ->selectCreatedDate()
      ->addOpen()
      ->addCritical()
    ;

    $this->queries['80x_open_criticals_promoted'] = new IssueQuery();
    $this->queries['80x_open_criticals_promoted']
      ->selectDistinctNID()
      ->selectCreatedDate()
      ->selectChangedDate()
      ->addOpen()
      ->addCritical()
      ->newPriority(array(IssueQuery::CRITICAL))
    ;

    $this->queries['80x_all'] = new IssueQuery();
    $this->queries['80x_all']
      ->selectDistinctNID()
      ->selectCreatedDate()
      ->selectFixedDate()
      ->addFixed()
      ->fixedAfter('2011-03-09')
      ->orderBy(array('fixed_date'))
    ;

    $this->queries['7x_criticals'] = new IssueQuery();
    $this->queries['7x_criticals']
      ->selectDistinctNID()
      ->selectCreatedDate()
      ->selectFixedDate()
      ->add7x()
      ->addFixed()
      ->addCritical()
      ->fixedBefore('2011-07-01')
      ->fixedAfter('2008-02-01')
      ->orderBy(array('fixed_date'))
    ;

  $this->queries['7x_criticals_promoted'] = new IssueQuery();
  $this->queries['7x_criticals_promoted']
      ->selectDistinctNID()
      ->selectCreatedDate()
      ->selectFixedDate()
      ->selectChangedDate()
      ->add7x()
      ->addFixed()
      ->addCritical()
      ->newPriority(array(IssueQuery::CRITICAL))
      ->fixedBefore('2011-03-09')
      ->fixedAfter('2008-02-01')
      ->orderBy(array('fixed_date'))
    ;

    $this->queries['7x_all'] = new IssueQuery();
    $this->queries['7x_all']
      ->selectDistinctNID()
      ->selectCreatedDate()
      ->selectFixedDate()
      ->add7x()
      ->fixedBefore('2011-03-09')
      ->fixedAfter('2008-02-01')
      ->orderBy(array('fixed_date'))
    ;

/*
// Backport queries are broken.
    $this->queries['80x_critical_backports'] = new IssueQuery();
    $this->queries['80x_critical_backports']
      ->selectDistinctNID()
      ->selectBackportedFrom80xDate()
      ->add80xto7xBackport()
      ->addCritical()
    ;

    $this->queries['80x_all_backports'] = new IssueQuery();
    $this->queries['80x_all_backports']
      ->selectDistinctNID()
      ->selectBackportedFrom80xDate()
      ->add80xto7xBackport()
    ;

    $this->queries['7x_critical_backports'] = new IssueQuery();
    $this->queries['7x_critical_backports']
      ->selectDistinctNID()
      ->selectBackportedFrom7xDate()
      ->add7xto6xBackport()
      ->addCritical()
    ;

    $this->queries['7x_all_backports'] = new IssueQuery();
    $this->queries['7x_all_backports']
      ->selectDistinctNID()
      ->selectBackportedFrom7xDate()
      ->add7xto6xBackport()
    ;
*/
  }

}
