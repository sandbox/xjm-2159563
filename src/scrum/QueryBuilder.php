<?php

namespace Drupal\core_metrics\scrum;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {
    // @todo: This is a fixed string at the moment.
    $this->queries['dbdate'] = new IssueQuery();
    $this->queries['dbdate']->setStaticQuery("select date_sub((select from_unixtime(changed) from node order by changed desc limit 1), interval 7 day) as start_date, (select from_unixtime(changed) from node order by changed desc limit 1) as end_date;")
      ;
    // @todo: This is being included from a file at the moment.
    $this->queries['demoted_criticals'] = new IssueQuery();
    $this->queries['demoted_criticals']
      ->setStaticQuery(file_get_contents(__DIR__ . '/../../scrum/demoted_criticals.txt'))
      ;
    $this->queries['fixed_all'] = new IssueQuery();
    $this->queries['fixed_all']
      ->selectCount()
      ->addFixed()
      ->fixedLastWeek()
      ;
    $this->queries['fixed_criticals'] = new IssueQuery();
    $this->queries['fixed_criticals']
      ->addFixed()
      ->addCritical()
      ->fixedLastWeek()
      ;
    $this->queries['new_fixed_criticals'] = new IssueQuery();
    $this->queries['new_fixed_criticals']
      ->addFixed()
      ->addCritical()
      ->createdLastWeek()
      ->fixedLastWeek()
      ;
    $this->queries['new_unfixed_criticals'] = new IssueQuery();
    $this->queries['new_unfixed_criticals']
      ->addOpen()
      ->addCritical()
      ->createdLastWeek()
      ;
    $this->queries['octo_all'] = new IssueQuery();
    $this->queries['octo_all']
      ->selectCount()
      ->addFixed()
      ->addOCTO()
      ->fixedLastWeek()
      ;
    $this->queries['octo_criticals'] = new IssueQuery();
    $this->queries['octo_criticals']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->addOCTO()
      ->fixedLastWeek()
      ;
    $this->queries['open_criticals'] = new IssueQuery();
    $this->queries['open_criticals']
      ->selectCount()
      ->addOpen()
      ->addCritical()
      ;
    $this->queries['open_d8up'] = new IssueQuery();
    $this->queries['open_d8up']
      ->selectCount()
      ->addOpen()
      ->addCritical()
      ->addD8up()
      ;
    $this->queries['fixed_d8up'] = new IssueQuery();
    $this->queries['fixed_d8up']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->addD8up()
      ->fixedLastWeek()
      ;
    $this->queries['promoted_fixed_criticals'] = new IssueQuery();
    $this->queries['promoted_fixed_criticals']
      ->addFixed()
      ->addCritical()
      ->movedIntoQueue()
      ->createdBeforeLastWeek()
      ->changedLastWeek()
      ->fixedLastWeek()
      ;
    $this->queries['promoted_unfixed_criticals'] = new IssueQuery();
    $this->queries['promoted_unfixed_criticals']
      ->addOpen()
      ->addCritical()
      ->movedIntoQueue()
      ->createdBeforeLastWeek()
      ->changedLastWeek()
      ;
    $this->queries['reopened_criticals'] = new IssueQuery();
    $this->queries['reopened_criticals']
      ->addOpen()
      // @todo This does not guarantee it was critical before it was reopened,
      // just that it is critical now and was once fixed.
      ->oldStatus(array(IssueQuery::FIXED, IssueQuery::CLOSED_FIXED))
      ->changedLastWeek()
      ->addCritical()
      ;
    $this->queries['total_criticals'] = new IssueQuery();
    $this->queries['total_criticals']
      ->selectCount()
      ->addCritical()
      ;
    $this->queries['total_fixed_d8up'] = new IssueQuery();
    $this->queries['total_fixed_d8up']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->addD8up()
      ->fixedAfter('2014-10-01')
      ;
  }

}
