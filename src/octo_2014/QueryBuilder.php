<?php

namespace Drupal\core_metrics\octo_2014;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {
    $this->queries['fixed_criticals'] = new IssueQuery();
    $this->queries['fixed_criticals']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->fixedBefore('2015-01-01')
      ->fixedAfter('2014-01-01')
      ;
    $this->queries['octo_criticals'] = new IssueQuery();
    $this->queries['octo_criticals']
      ->selectDistinctNID()
      ->addFixed()
      ->addCritical()
      ->addOCTO(array('xjm', 'effulgentsia', 'wim', 'gabor'))
      ->octoTimeframe('2013-10-01', '2015-01-01')
      ->fixedBefore('2015-01-01')
      ->fixedAfter('2014-01-01')
      ;
    $this->queries['jesse_criticals'] = new IssueQuery();
    $this->queries['jesse_criticals']
      ->selectDistinctNID()
      ->addFixed()
      ->addCritical()
      ->addOCTO(array('jessebeach'))
      ->octoTimeframe('2013-10-01', '2014-06-19')
      ->fixedBefore('2015-01-01')
      ->fixedAfter('2014-01-01')
      ;
    $this->queries['tim_criticals'] = new IssueQuery();
    $this->queries['tim_criticals']
      ->selectDistinctNID()
      ->addFixed()
      ->addCritical()
      ->addOCTO(array('timplunkett'))
      ->octoTimeframe('2014-06-15', '2015-01-01')
      ->fixedBefore('2015-01-01')
      ->fixedAfter('2014-06-15')
      ;
    $this->queries['devin_criticals'] = new IssueQuery();
    $this->queries['devin_criticals']
      ->selectDistinctNID()
      ->addFixed()
      ->addCritical()
      ->addOCTO(array('devin'))
      ->octoTimeframe('2014-08-19', '2015-01-01')
      ->fixedBefore('2015-01-01')
      ->fixedAfter('2014-08-19')
      ;

  }

}
