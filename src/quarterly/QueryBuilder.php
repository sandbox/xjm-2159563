<?php

namespace Drupal\core_metrics\quarterly;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  protected $startDate = '2015-04-01';

  protected $endDate = '2015-07-01';

  public function __construct() {

    $this->queries['fixed_criticals'] = new IssueQuery();
    $this->queries['fixed_criticals']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->fixedAfter($this->startDate)
      ->fixedBefore($this->endDate)
      ;
    $this->queries['new_criticals'] = new IssueQuery();
    $this->queries['new_criticals']
      ->selectCount()
      ->addCritical()
      ->createdAfter($this->startDate)
      ->createdBefore($this->endDate)
      ;
    $this->queries['open_criticals'] = new IssueQuery();
    $this->queries['open_criticals']
      ->selectCount()
      ->addOpen()
      ->addCritical()
      ;
    $this->queries['open_d8up'] = new IssueQuery();
    $this->queries['open_d8up']
      ->selectCount()
      ->addOpen()
      ->addCritical()
      ->addD8up()
      ;
    $this->queries['fixed_d8up'] = new IssueQuery();
    $this->queries['fixed_d8up']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->addD8up()
      ->fixedAfter('2014-10-01')
      ->fixedBefore($this->endDate)
      ;
    $this->queries['octo_criticals'] = new IssueQuery();
    $this->queries['octo_criticals']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->addOCTO()
      ->fixedBefore($this->endDate)
      ->fixedAfter($this->startDate)
      ;
  }

}
