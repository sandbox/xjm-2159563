<?php

namespace Drupal\core_metrics\webchick;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {

    $this->queries['webchick'] = new IssueQuery();
    $this->queries['webchick']
      ->selectDistincNID()
      ->addRTBC()
      ;

  }

}
