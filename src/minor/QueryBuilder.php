<?php

namespace Drupal\core_metrics\minor;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {
    $this->queries['open_committed_to_prep'] = new IssueQuery();
    $this->queries['open_committed_to_prep']
      ->addOpen()
      ->addStable()
      ->movedFromBranch(IssueQuery::PREP)
      ;

  }

}
