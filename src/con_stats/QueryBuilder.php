<?php

namespace Drupal\core_metrics\con_stats;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {
    $start_date = '2015-05-09 00:00:00';
    $end_date = '2015-05-19 00:00:00';

    // @todo: This is a fixed string at the moment.
    $this->queries['dbdate'] = new IssueQuery();
    $this->queries['dbdate']->setStaticQuery("select date_sub((select from_unixtime(changed) from node order by changed desc limit 1), interval 7 day) as start_date, (select from_unixtime(changed) from node order by changed desc limit 1) as end_date;")
      ;
    // @todo: This is being included from a file at the moment.
//    $this->queries['demoted_criticals'] = new IssueQuery();
//    $this->queries['demoted_criticals']
//      ->setStaticQuery(file_get_contents(__DIR__ . '/../../scrum/demoted_criticals.txt'))
      ;
    $this->queries['fixed_all'] = new IssueQuery();
    $this->queries['fixed_all']
      ->selectCount()
      ->addFixed()
      ->fixedAfter($start_date)
      ->fixedBefore($end_date)
      ;
    $this->queries['fixed_criticals'] = new IssueQuery();
    $this->queries['fixed_criticals']
      ->addFixed()
      ->addCritical()
      ->fixedAfter($start_date)
      ->fixedBefore($end_date)
      ;
    $this->queries['new_fixed_criticals'] = new IssueQuery();
    $this->queries['new_fixed_criticals']
      ->addFixed()
      ->addCritical()
      ->fixedAfter($start_date)
      ->fixedBefore($end_date)
      ->createdAfter($start_date)
      ->createdBefore($end_date)
      ;
    $this->queries['new_unfixed_criticals'] = new IssueQuery();
    $this->queries['new_unfixed_criticals']
      ->addOpen()
      ->addCritical()
      ->createdAfter($start_date)
      ->createdBefore($end_date)
      ;
    $this->queries['promoted_fixed_criticals'] = new IssueQuery();
    $this->queries['promoted_fixed_criticals']
      ->addFixed()
      ->addCritical()
      ->movedIntoQueue()
      ->createdAfter($start_date)
      ->createdBefore($end_date)
      ->changedAfter($start_date)
      ->changedBefore($end_date)
      ->fixedLastWeek()
      ;
    $this->queries['promoted_unfixed_criticals'] = new IssueQuery();
    $this->queries['promoted_unfixed_criticals']
      ->addOpen()
      ->addCritical()
      ->movedIntoQueue()
      ->createdAfter($start_date)
      ->createdBefore($end_date)
      ->changedAfter($start_date)
      ->changedBefore($end_date)
      ;
    $this->queries['issues_updated'] = new IssueQuery();
    $this->queries['issues_updated']
      ->changedAfter($start_date)
      ->changedBefore($end_date)
      ;
    $this->queries['criticals_updated'] = new IssueQuery();
    $this->queries['criticals_updated']
      ->addCritical()
      ->changedAfter($start_date)
      ->changedBefore($end_date)
      ;
    $this->queries['majors_updated'] = new IssueQuery();
    $this->queries['majors_updated']
      ->addMajor()
      ->changedAfter($start_date)
      ->changedBefore($end_date)
      ;
    $this->queries['reopened_criticals'] = new IssueQuery();
    $this->queries['reopened_criticals']
      ->addOpen()
      // @todo This does not guarantee it was critical before it was reopened,
      // just that it is critical now and was once fixed.
      ->oldStatus(array(IssueQuery::FIXED, IssueQuery::CLOSED_FIXED))
      ->changedAfter($start_date)
      ->changedBefore($end_date)
      ->addCritical()
      ;
  }

}
