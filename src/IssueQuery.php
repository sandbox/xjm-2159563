<?php

namespace Drupal\core_metrics;

/**
 * Builds a query for Drupal.org issues.
 */
class IssueQuery {

  /**
   * A subquery to get the date one week before the DB was last updated.
   *
   * The staging database is only updated once a day, with data that is more
   * than a day old. Using this subquery for weekly queries ensures a full
   * week's worth of data.
   */
  const WEEK_AGO = 'date_sub((select from_unixtime(changed) from node order by changed desc limit 1), interval 7 day)';

  const CORE = 3060;

  const ACTIVE = 1;
  const NW = 13;
  const NR = 8;
  const RTBC = 14;
  const POSTPONED = 4;
  const FIXED = 2;
  const CLOSED_FIXED = 7;

  const CRITICAL = 400;
  const MAJOR = 300;
  const NORMAL = 200;
  const MINOR = 100;

  const BUG = 1;
  const TASK = 2;
  const PLAN = 5;
  const FEATURE = 3;

  /**
   * The current (stable) minor release branch.
   */
  const STABLE = '9.1.x-dev';

  /**
   * The next (developmental) minor release branch.
   */
  const DEV = '9.2.x-dev';

  /**
   * The upcoming (alpha, beta, or RC) minor release branch in preparation.
   */
  const PREP = '9.2.x-dev';

  /**
   * An array of user IDs, keyed by a short name.
   *
   * @var int[]
   */
  protected $uids = array();

  /**
   * A string containing a static query to use instead of the builder.
   *
   * This proprty allows statically defining a query that the builder cannot
   * build yet.
   */
  protected $staticQuery;

  /**
   * Sets a static query to use instead of the builder.
   *
   * @todo This proprty allows statically defining a query that the builder
   *  cannot build yet. The property and method should be removed eventually.
   *
   * @param string $string
   *   A string containing the SQL query.
   */
  public function setStaticQuery($string) {
    $this->staticQuery = $string;
    return $this;
  }

  /**
   * The fields to select.
   *
   * @var string[]
   */
  protected $select = array();

  /**
   * The joins for the query.
   *
   * Each join is given an alias (the array key) to ensure that duplicate joins
   * will only be added deliberately (e.g., when adding multiple joins for
   * different aliases of the same table). The caller should ensure that the
   * joins have the correct aliases to match any conditions, selected fields,
   * ordering fields, etc.
   *
   * @var string[]
   */
  protected $joins = array();

  /**
   * An array of subqueries, keyed by a label for the subquery.
   *
   * Each element has the following structure:
   * - query_label: An associative array:
   *   - select: The fields to select for the subquery.
   *   - joins: The joins for the subquery (to add onto the base joins).
   *   - conditions: The conditions for the subquery.
   *
   * @var array[]
   *
   * @todo This isn't used. See IssueQuery::$commentData.
   */
  protected $subqueries = array();

  /**
   * Conditions to add to the end of the query.
   *
   * Each condition is given an alias (the array key) to ensure that duplicates
   * will only be added deliberately (e.g., when adding the condition for
   * different aliases of the same table). Conditions may use any of the
   * following tables:
   * - the base joins from IssueQuery::baseJoins()
   * - the joins added from IssueQuery::$joins
   * - 'node_comments' for the comment data
   * - 'node_files' for the patch data
   * Make sure the conditions use table aliases that match those provided in
   * IssueQuery::$joins, if appropriate.
   *
   * Don't include conditions that need to be calculated when the query is
   * built (e.g. statuses).
   *
   * @var string[]
   */
  protected $conditions = array();

  /**
   * The projects to query.
   *
   * @var int[]
   */
  protected $project = array();

  /**
   * The versions to query.
   *
   * @var string[]
   */
  protected $version = array();

  /**
   * The issue statuses to query.
   *
   * @var int[]
   */
  protected $status = array();

  /**
   * The issue categories to query.
   *
   * @var int[]
   */
  protected $category = array();

  /**
   * The issue priorities to query.
   *
   * @var int[]
   */
  protected $priority = array();

  /**
   * List of taxonomy terms that the issues should not be tagged with.
   *
   * @var int[]
   */
  protected $withoutTaxonomyTerm = array();

  /**
   * Dates relevant to the query.
   *
   * @var string[]
   */
  protected $dates = array();

  protected $movedIntoQueue = FALSE;

  protected $groupBy = array();

  protected $orderBy = array('n.nid');

  protected $limit;

  protected $commentData = FALSE;

  protected $patchData = FALSE;

  /**
   * Serialized values for the changes field.
   *
   * An associative array of strings matching the serialized format used in the
   * changes field, keyed first by the field type, then the desired value in
   * the corresponding field table.
   */
  protected $changeStrings = array();

  public function __construct() {
    $this->changeStrings = array(
      'project' => array(
        static::CORE => 'a:1:{i:0;a:1:{s:9:"target_id";s:4:"3060";}}',
      ),
      'version' => array(
        '8.0.x-dev' => 'a:1:{i:0;a:1:{s:5:"value";s:9:"8.0.x-dev";}}',
        '8.1.x-dev' => 'a:1:{i:0;a:1:{s:5:"value";s:9:"8.1.x-dev";}}',
        '8.2.x-dev' => 'a:1:{i:0;a:1:{s:5:"value";s:9:"8.2.x-dev";}}',
        '8.3.x-dev' => 'a:1:{i:0;a:1:{s:5:"value";s:9:"8.3.x-dev";}}',
        '8.4.x-dev' => 'a:1:{i:0;a:1:{s:5:"value";s:9:"8.4.x-dev";}}',
        '8.5.x-dev' => 'a:1:{i:0;a:1:{s:5:"value";s:9:"8.5.x-dev";}}',
        '8.6.x-dev' => 'a:1:{i:0;a:1:{s:5:"value";s:9:"8.6.x-dev";}}',
        '8.7.x-dev' => 'a:1:{i:0;a:1:{s:5:"value";s:9:"8.7.x-dev";}}',
        '8.8.x-dev' => 'a:1:{i:0;a:1:{s:5:"value";s:9:"8.8.x-dev";}}',
        '8.9.x-dev' => 'a:1:{i:0;a:1:{s:5:"value";s:9:"8.9.x-dev";}}',
        '9.0.x-dev' => 'a:1:{i:0;a:1:{s:5:"value";s:9:"9.0.x-dev";}}',
        '9.1.x-dev' => 'a:1:{i:0;a:1:{s:5:"value";s:9:"9.1.x-dev";}}',
        '9.2.x-dev' => 'a:1:{i:0;a:1:{s:5:"value";s:9:"9.2.x-dev";}}',
        '7.x-dev' => 'a:1:{i:0;a:1:{s:5:"value";s:7:"7.x-dev";}}',
      ),
      'category' => array(
        static::BUG => 'a:1:{i:0;a:1:{s:5:"value";s:1:"1";}}',
        static::TASK => 'a:1:{i:0;a:1:{s:5:"value";s:1:"2";}}',
        static::PLAN => 'a:1:{i:0;a:1:{s:5:"value";s:1:"5";}}',
        static::FEATURE => 'a:1:{i:0;a:1:{s:5:"value";s:1:"3";}}',
      ),
      'priority' => array(
        static::CRITICAL => 'a:1:{i:0;a:1:{s:5:"value";s:3:"400";}}',
        static::MAJOR => 'a:1:{i:0;a:1:{s:5:"value";s:3:"300";}}',
        static::NORMAL => 'a:1:{i:0;a:1:{s:5:"value";s:3:"200";}}',
        static::MINOR => 'a:1:{i:0;a:1:{s:5:"value";s:3:"100";}}',
      ),
      'status' => array(
        static::ACTIVE => 'a:1:{i:0;a:1:{s:5:"value";s:1:"1";}}',
        static::NW => 'a:1:{i:0;a:1:{s:5:"value";s:2:"13";}}',
        static::NR => 'a:1:{i:0;a:1:{s:5:"value";s:1:"8";}}',
        static::RTBC => 'a:1:{i:0;a:1:{s:5:"value";s:2:"14";}}',
        static::POSTPONED => 'a:1:{i:0;a:1:{s:5:"value";s:1:"4";}}',
        static::FIXED => 'a:1:{i:0;a:1:{s:5:"value";s:1:"2";}}',
        static::CLOSED_FIXED => 'a:1:{i:0;a:1:{s:5:"value";s:1:"7";}}',
      ),
      'tags' => array(
        'triaged' => '%Triaged D8 critical%',
        'triaged_critical' => '%Triaged D8 critical%',
        'triaged_major' => '%Triaged D8 major%',
        'fm_review' => '%Needs framework manager review%',
        'rm_review' => '%Needs release manager review%',
        'pm_review' => '%Needs product manager review%',
      ),
    );

    $this->tids = array(
      'triaged_critical' => 164349,
      'triaged_major' => 174642,
      'critical_triage_deferred' => 169071,
      'major_triage_deferred' => 177412,
      'major_current_state' => 177626,
      'fm_review' => 169963,
      'pm_review' => 170004,
      'rm_review' => 171496,
      'd8up' => 27290,
      'd7backport' => 21556,
      'd6backport' => 182,
      'vdc' => 36416,
      'twig' => 36330,
      'entity' => 38578,
      'blocker' => 38080,
      'api_first' => 177096,
    );

    $this->uids = array(
      'dries' => 1,
      'alexpott' => 157725,
      'catch' => 35733,
      'cilefen' => 1850070,
      'cottser' => 1167326,
      'devin' => 290182,
      'drpal' => 1602706,
      'effulgentsia' => 78040,
      'gabor' => 4166,
      'jessebeach' => 748566,
      'larowlan' => 395439,
      'lauriii' => 1078742,
      'moshe' => 23,
      'plach' => 183211,
      'timplunkett' => 241634,
      'webchick' => 24967,
      'wim' => 99777,
      'xjm' => 65776,
      'yoroy' => 41502,
      'system_message' => 180064,
    );

    $this->fm = array('alexpott', 'effulgentsia', 'catch', 'cottser', 'larowlan', 'lauriii', 'plach');
    $this->rm = array('catch', 'xjm', 'cilefen');
    $this->pm = array('dries', 'webchick', 'yoroy', 'gabor');

    $this->committers = array_unique(array_merge($this->fm, $this->rm, $this->pm));

    $this->octo = array('Dries', 'gabor', 'effulgentsia', 'webchick', 'wim', 'xjm', 'timplunkett', 'prestonso', 'tedbow', 'drpal');

  }

  public function add80x() {
    $this->version[] = '8.0.x-dev';
    return $this;
  }

  public function addDev() {
    $this->version[] = static::DEV;
    return $this;
  }

  public function addStable() {
    $this->version[] = static::STABLE;
    return $this;
  }

  public function addPrep() {
    $this->version[] = static::PREP;
    return $this;
  }

  /**
   * Adds a join for a particular taxonomy term.
   *
   * @param string $name
   *   The shorthand name for the tag. See self::$tids for known values.
   * @param bool $inner
   *   (optional) Whether to use an inner join (TRUE) or left join (FALSE).
   *   Defaults to TRUE.
   *
   * @return $this
   */
  public function addTag($name, $inner = TRUE) {
    $name = static::escapeString($name);
    $this->joins[$name] = static::taxonomyTermJoin($this->tids[$name], $name, $inner);
    return $this;
  }

  /**
   * Adds a subquery to only select issues without a particular taxonomy term.
   *
   * @param string $name
   *   The shorthand name for the tag. See self::$tids for known values.
   *
   * @return $this
   */
  public function withoutTag($name) {
    $name = static::escapeString($name);
    $this->withoutTaxonomyTerm[$name] = $this->tids[$name];
  }

  public function add80xto7xBackport() {
    $this->version[] = '7.x-dev';
    $this->addTag('d7backport');
    // @todo A change join with the previous values as 8.x... and PTBP? check.
    return $this;
  }

  public function add7xto6xBackport() {
    $this->version[] = '6.x-dev';
    $this->addTag('d6backport');
    // @todo A change join with the previous values as 7.x... and PTBP? check.
    return $this;
  }

  public function add7x() {
    $this->version[] = '7.x-dev';
    return $this;
  }

  public function add6x() {
    $this->version[] = '6.x-dev';
    return $this;
  }

  public function addFixed() {
    $this->status = array_merge($this->status, array(static::FIXED, static::CLOSED_FIXED));
    return $this;
  }

  public function addRTBC() {
    $this->status[] = static::RTBC;
    return $this;
  }

  public function addOpen() {
    $this->status = array_merge($this->status, array(static::ACTIVE, static::NW, static::NR, static::RTBC, static::POSTPONED));
    return $this;
  }

  public function addBug() {
    $this->category[] = static::BUG;
    return $this;
  }

  public function addTask() {
    $this->category[] = static::TASK;
    return $this;
  }

  public function addPlan() {
    $this->category[] = static::PLAN;
    return $this;
  }


  public function addFeature() {
    $this->category[] = static::FEATURE;
    return $this;
  }

  public function addCritical() {
    $this->priority[] = static::CRITICAL;
    return $this;
  }

  public function addMajor() {
    $this->priority[] = static::MAJOR;
    return $this;
  }

  public function addNormal() {
    $this->priority[] = static::NORMAL;
    return $this;
  }

  public function addMinor() {
    $this->priority[] = static::MINOR;
    return $this;
  }

  /**
   * Limits the query to certain components.
   *
   * @param string[] $components
   *   List of the component names to select.
   */
  public function addComponents(array $components) {
    foreach ($components as $i => $component) {
      $components[$i] = "'" . static::escapeString($component) . "'";
    }
    $this->joins['component'] = 'inner join field_data_field_issue_component cmp on cmp.entity_id = n.nid';
    $this->conditions[] = 'and cmp.field_issue_component_value in (' . implode(', ', $components) . ')';
    return $this;
  }



  public function movedIntoQueue() {
    $this->joins['changed'] = static::changeJoins();
    $this->movedIntoQueue = TRUE;
    return $this;
  }

  public function selectDistinctNID() {
    $this->select[] = 'distinct n.nid';
    return $this;
  }

  public function selectNID() {
    $this->select[] = 'n.nid';
    return $this;
  }

  public function selectCount() {
    $this->select[] = 'count(distinct n.nid) as node_count';
    return $this;
  }

  public function selectTitle() {
    // Strip double quotes from the title to avoid CSV-parsing annoyances.
    $this->select[] = "replace(n.title, '\"', '') as title";
    return $this;
  }

  public function selectOwner() {
    $this->select[] = 'assigned_u.name';
    $this->joins[] = 'inner join field_data_field_issue_assigned assigned on assigned.entity_id = n.nid';
    $this->joins[] = 'inner join users assigned_u on assigned_u.uid = assigned.field_issue_assigned_target_id';
    return $this;
  }

  public function selectAge() {
    $this->select[] = 'datediff(now(), from_unixtime(n.created)) as age';
    return $this;
  }

  public function selectCreatedDate() {
    $this->select[] = "from_unixtime(n.created, '%Y-%m-%d') as created_date";
    return $this;
  }

  public function selectUpdated() {
    $this->select[] = 'datediff(now(), from_unixtime(node_comments.recent_comment_date)) as last_update';
    $this->commentData = TRUE;
    return $this;
  }

  public function selectCommentCount() {
    $this->select[] = 'node_comments.comment_count';
    $this->commentData = TRUE;
    return $this;
  }

  public function selectCommenters() {
    $this->select[] = 'node_comments.commenters';
    $this->commentData = TRUE;
    return $this;
  }

  public function selectPatchUpdated() {
    $this->select[] = 'datediff(now(), from_unixtime(node_files.last_patch_date)) as patch_updated';
    $this->patchData = TRUE;
    return $this;
  }

  public function selectComponent() {
    $this->select[] = 'cmp.field_issue_component_value as component';
    $this->joins['component'] = 'inner join field_data_field_issue_component cmp on cmp.entity_id = n.nid';
    return $this;
  }

  public function selectStatus() {
    $this->select[] = 's.field_issue_status_value as status';
    return $this;
  }

  public function addCore() {
    $this->project[] = static::CORE;
    return $this;
  }

  public function addOCTO(array $users = array()) {
    $this->joins['octo'] = $this->octoJoins($users);
    $this->conditions['octo'] = "and ((char_length(octo_cb.comment_body_value) > 500) or (octo_files.filename like '%.patch'))";
    // @todo Make this the date range for the query, if any.
    $this->conditions['octo_time'] = "and octo_c.changed > unix_timestamp(date_sub(now(), interval 180 day))";
    return $this;
  }

  public function addCommenterTimeframe(array $users, $start_date, $end_date, $alias = 'commenter') {
    $uid_string = implode(', ', $this->getUids($users));
    $this->joins[$alias] = "inner join comment {$alias}_c on {$alias}_c.nid = n.nid and {$alias}_c.uid in ({$uid_string})";
    $this->conditions[$alias] = " and {$alias}_c.changed <= unix_timestamp(" . $this->sanitizeDate($end_date) . ") and {$alias}_c.changed > unix_timestamp(" . $this->sanitizeDate($start_date) . ")";
    return $this;
  }

  public function selectFixedWeekCount() {
    $this->select[] = 'count(distinct n.nid) as node_count';
    $this->select[] = 'yearweek(from_unixtime(fixed_c.changed)) as fixed_week';
    $this->conditions['fixed_week'] = static::andC($this->changesNewCondition('status', array(static::FIXED), FALSE, 'fixed'));
    $this->groupBy[] = 'fixed_week';
    $this->orderBy = array('fixed_week');
    return $this;
  }

  /**
   * Must be used with a changes condition to really be meaningful.
   *
   * @return $this
   */
  public function selectChangedDate() {
    $this->joins['changed'] = static::changeJoins('changed');
    $this->select[] = "from_unixtime(changed_c.changed, '%Y-%m-%d') as changed_date";
    return $this;
  }

  public function selectFixedDate() {
    $this->select[] = "from_unixtime(fixed_c.changed, '%Y-%m-%d') as fixed_date";
    $this->conditions['fixed'] = static::andC($this->changesNewCondition('status', array(static::FIXED), FALSE, 'fixed'));
    return $this;
  }

  public function selectQueueMovedDate() {
    $this->select[] = "from_unixtime(changed_c.changed, '%Y-%m-%d') as changed_date";
    $this->movedIntoQueue();
    return $this;
  }

  public function selectBackportedFrom80xDate() {
    $this->select[] = "from_unixtime(backported_c.changed, '%Y-%m-%d') as backported_date";
    $this->conditions['backported'] = static::andC($this->changesOldCondition('version', array('8.0.x-dev'), FALSE, 'backported'));
    return $this;

  }

  public function selectBackportedFrom7xDate() {
    $this->select[] = "from_unixtime(backported_c.changed, '%Y-%m-%d') as backported_date";
    $this->conditions['backported'] = static::andC($this->changesOldCondition('version', array('7.x-dev'), FALSE, 'backported'));
    return $this;
  }

  public function selectTriagedTags() {
    $this->select[] = "triaged_critical_ti.tid is not null as triaged";
    $this->select[] = "critical_triage_deferred_ti.tid is not null as triage_deferred";
    $this->addTag('triaged_critical', FALSE);
    $this->addTag('critical_triage_deferred', FALSE);
    return $this;
  }

  /**
   * @todo This only works if run after addOCTO().
   */
  public function octoTimeframe($start_date, $end_date) {
    if (!empty($this->conditions['octo_time'])) {
      $this->conditions['octo_time'] = "and octo_c.changed <= unix_timestamp(" . $this->sanitizeDate($end_date) . ") and octo_c.changed > unix_timestamp(" . $this->sanitizeDate($start_date) . ")";
    }
    return $this;
  }

  public function addMaintainer(array $users = array()) {
    if (empty($users)) {
      $users = $this->committers;
    }

    $this->joins['maintainer'] = $this->commenterJoins($users, 'maintainer');
    return $this;
  }

  /**
   * @param array $fields
   * @return $this
   */
  public function orderBy(array $fields) {
    foreach ($fields as $i => $item) {
      $fields[$i] = static::escapeString($item);
    }
    $this->orderBy = $fields;
    return $this;
  }

  /**
   * @param array $field
   * @return $this
   */
  public function groupBy($fields) {
    foreach ($fields as $i => $item) {
      $fields[$i] = static::escapeString($item);
    }
    $this->groupBy = $fields;
    return $this;
  }

  /**
   * @param int $limit
   *
   * @return $this
   */
  public function limit($limit = NULL) {
    $this->limit = NULL;
    if (!empty($limit)) {
      $this->limit = (int) $limit;
    }
    return $this;
  }

  protected static function escapeTableName($string) {
    return preg_replace('/[^A-Za-z0-9_.]+/', '', $string);
  }

  protected static function escapeString($string) {
    return preg_replace('/[^A-Za-z0-9_. -]+/', '', $string);
  }

  protected static function escapeVersionNumber($string) {
    return preg_replace('/[^0-9_.]+/', '', $string);
  }

  public function addStringChange($version) {
    $version = static::escapeVersionNumber($version);
    $this->joins['string_change'] = static::taxonomyTermNameJoin("String change in $version", 'string_change');
    return $this;
  }

  public function addReleaseNotes($version) {
    $version = static::escapeVersionNumber($version);
    $this->joins['string_change'] = static::taxonomyTermNameJoin("$version release notes", 'string_change');
    return $this;
  }

  public function addD8up() {
    $this->addTag('d8up');
    return $this;
  }

  public function addNotD8up() {
    $this->withoutTag('d8up');
    return $this;
  }

  public function addTriaged() {
    $this->addTag('triaged_critical');
    return $this;
  }

  public function addTriageDeferred() {
    $this->addTag('critical_triage_deferred');
    return $this;
  }

  public function addCurrentState() {
    $this->addTag('major_current_state');
    return $this;
  }

  public function addNotTriaged() {
    $this->withoutTag('triaged_critical');
    $this->withoutTag('critical_triage_deferred');
    return $this;
  }

  public function addMajorNotTriaged() {
    $this->withoutTag('triaged_major');
    $this->withoutTag('major_triage_deferred');
    return $this;
  }

  public function addBase() {
    $components = array('views.module', 'views_ui.module');
    $component ='base system';
    $component = "'" . static::escapeString($component) . "'";
    $this->joins['component'] = 'inner join field_data_field_issue_component cmp on cmp.entity_id = n.nid';
    $this->conditions['base'] = static::andC(
      'cmp.field_issue_component_value in (' .  $component . ')');
    return $this;
  }

  public function addVDC() {
    $components = array('views.module', 'views_ui.module');
    foreach ($components as $i => $component) {
      $components[$i] = "'" . static::escapeString($component) . "'";
    }
    $this->joins['component'] = 'inner join field_data_field_issue_component cmp on cmp.entity_id = n.nid';
    $this->addTag('vdc', FALSE);
    // @todo Update to not hardcode tid below?
    $this->conditions['vdc'] = static::andC(
      'cmp.field_issue_component_value in (' . implode(', ', $components) . ')'
      .  static::orC('vdc_ti.tid = 36416'));
    return $this;
  }

  public function addTheme() {
    $components = array('theme system', 'render system', 'asset library system');
    foreach ($components as $i => $component) {
      $components[$i] = "'" . static::escapeString($component) . "'";
    }
    $this->joins['component'] = 'inner join field_data_field_issue_component cmp on cmp.entity_id = n.nid';
    $this->addTag('twig', FALSE);
    // @todo Update to not hardcode tid below?
    $this->conditions['twig'] = static::andC(
      'cmp.field_issue_component_value in (' . implode(', ', $components) . ')'
      .  static::orC('twig_ti.tid = 36330'));
    return $this;
  }

  public function addAPIFirstBlocker() {
    $this->addTag('api_first');
    $this->addTag('blocker');
    return $this;
  }

  public function addREST() {
    $components = array('rest.module', 'serialization.module');
    foreach ($components as $i => $component) {
      $components[$i] = "'" . static::escapeString($component) . "'";
    }
    $this->joins['component'] = 'inner join field_data_field_issue_component cmp on cmp.entity_id = n.nid';
    // @todo Update to not hardcode tid below?
    $this->conditions['twig'] = static::andC(
      'cmp.field_issue_component_value in (' . implode(', ', $components) . ')'
//      .  static::orC('twig_ti.tid = 36330')
    );
    return $this;
  }

  public function addThemeRelated() {
    $this->addComponents(array('markup', 'CSS', 'Bartik theme', 'Classy theme', 'Stark theme', 'Seven theme', 'Stable theme'));
    return $this;
  }

  public function addEntity() {
    $components = array('entity system', 'field system', 'node system', 'entity_reference.module', 'field_ui.module', 'typed data system');
    foreach ($components as $i => $component) {
      $components[$i] = "'" . static::escapeString($component) . "'";
    }
    $this->joins['component'] = 'inner join field_data_field_issue_component cmp on cmp.entity_id = n.nid';
    $this->addTag('entity', FALSE);
    // @todo Update to not hardcode tid below?
    $this->conditions['entity'] = static::andC(
      'cmp.field_issue_component_value in (' . implode(', ', $components) . ')'
      .  static::orC('entity_ti.tid = 38578'));
    return $this;
  }

  // @todo Update with new tag alias?
  public function notTriagedByMaintainers() {
    $this->conditions['triaged'] = static::andC($this->changesNewCondition('tags', array('triaged'), FALSE, 'triaged'));
    $this->conditions['triaged'] .=  ' ' . static::andC($this->changesOldCondition('tags', array('triaged'), TRUE, 'triaged'));
    $this->conditions['triaged'] .=  ' ' . static::andC(static::in('triaged_c.uid', $this->getUids($this->committers), TRUE));
    return $this;
  }

  /**
   * Selects issues with a certain tag removed by certain maintainers.
   *
   * @param string $tag
   *   The shorthand name for the tag that was removed.
   * @param array $maintainers
   *   An array of shorthand maintainer usernames
   */
  public function maintainerUntag($tag, array $maintainers = array(), $alias = 'changed') {
    if (empty($maintainers)) {
      $maintainers = $this->committers;
    }

    $alias = static::escapeTableName($alias);
    $this->untag($tag, $alias);

    $this->conditions[$alias] .=  ' ' . static::andC(static::in("{$alias}_c.uid", $maintainers, TRUE));
    return $this;
  }

  /**
   * Selects issues with a certain tag removed.
   *
   * @param string $tag
   *   The shorthand name for the tag that was removed.
   * @param string $alias
   *   The table alias to use for the join.
   */
  public function untag($tag, $alias = 'changed') {
    $this->conditions[$alias] = static::andC($this->changesNewCondition('tags', array($tag), TRUE));
    $this->conditions[$alias] .=  ' ' . static::andC($this->changesOldCondition('tags', array($tag), FALSE));
    return $this;
  }

  public function maintainerUnRTBC(array $names = array()) {
    if (empty($names)) {
      $names = $this->committers;
    }
    $this->conditions['un_rtbc'] = static::andC($this->changesNewCondition('status', array(static::NW, static::NR)));
    $this->conditions['un_rtbc'] .=  ' ' . static::andC($this->changesOldCondition('status', array(static::RTBC)));
    $this->conditions['un_rtbc'] .=  ' ' . static::andC(static::in('changed_c.uid', $this->getUids($names)));
    return $this;
  }

  public function selectLastRTBC() {
    $this->joins['rtbced'] = static::changeJoins('rtbced');
    $this->select[] = "from_unixtime(max(rtbced_c.changed), '%Y-%m-%d') as rtbced_date";
    $this->conditions['rtbced'] = static::andC($this->changesNewCondition('status', array(static::RTBC), FALSE, 'rtbced'));
    $this->groupBy(array('n.nid'));
    return $this;
  }

  public function selectRTBCed() {
    $this->joins['changed'] = static::changeJoins();
    $this->select[] = "from_unixtime(max(changed_c.changed), '%Y-%m-%d') as rtbced_date";
    $this->conditions['changed'] = static::andC($this->changesNewCondition('status', array(static::RTBC)));
    return $this;
  }

  public function selectUnRTBCed() {
    $this->joins['changed'] = static::changeJoins();
    $this->select[] = "from_unixtime(max(changed_c.changed), '%Y-%m-%d') as un_rtbced_date";
    $this->conditions['changed'] = static::andC($this->changesNewCondition('status', array(static::NW, static::NR)));
    $this->conditions['changed'] .=  ' ' . static::andC($this->changesOldCondition('status', array(static::RTBC)));
    return $this;
  }

  /**
   * Creates a string to join for a given taxonomy term.
   *
   * @param $tid
   *   The taxonomy term to select.
   * @param string $alias
   *   The table alias to use for the join.
   * @param bool $inner
   *   (optional) Whether to use an inner join (TRUE) or left join (FALSE).
   *   Defaults to TRUE.
   *
   * @return string
   *   A join clause to add to $this->joins[] or elsewhere.
   */
  protected static function taxonomyTermJoin($tid, $alias = 'term', $inner = TRUE) {
    $tid = htmlspecialchars($tid);
    $alias = static::escapeTableName($alias);
    $join_type = $inner ? 'inner' : 'left';
    return "$join_type join taxonomy_index {$alias}_ti on n.nid = {$alias}_ti.nid and {$alias}_ti.tid = '$tid'";
  }

  /**
   * Creates a string to join for a given taxonomy term by name.
   *
   * @param $name
   *   The taxonomy term to select.
   * @param string $alias
   *   The table alias to use for the join.
   * @param bool $inner
   *   (optional) Whether to use an inner join (TRUE) or left join (FALSE).
   *   Defaults to TRUE.
   *
   * @return string
   *   A join clause to add to $this->joins[] or elsewhere.
   */
  protected static function taxonomyTermNameJoin($name, $alias = 'term', $inner = TRUE) {
    $name = htmlspecialchars($name);
    $alias = static::escapeTableName($alias);
    $join_type = $inner ? 'inner' : 'left';
    return "$join_type join taxonomy_term_data {$alias}_td on {$alias}_td.name LIKE '$name' $join_type join taxonomy_index {$alias}_ti on n.nid = {$alias}_ti.nid and {$alias}_ti.tid = {$alias}_td.tid";
  }

  public function createdBefore($date) {
    $this->conditions['created_before'] = ' and n.created <= unix_timestamp(' . $this->sanitizeDate($date) . ")";
    $this->dates['created_before'] = $date;
    return $this;
  }

  public function createdAfter($date) {
    $this->conditions['created_after'] = ' and n.created > unix_timestamp(' . $this->sanitizeDate($date) . ")";
    $this->dates['created_after'] = $date;
    return $this;
  }

  public function createdBeforeLastWeek() {
    $this->conditions['created_before'] = 'and n.created <= unix_timestamp(' . static::WEEK_AGO . ")";
    $this->dates['created_before'] = date('Y-m-d', strtotime('-1 week'));
    return $this;
  }

  public function createdLastWeek() {
    $this->conditions['created_after'] = 'and n.created > unix_timestamp(' . static::WEEK_AGO . ")";
    $this->dates['created_after'] = date('Y-m-d', strtotime('-1 week'));
    return $this;
  }

  public function changedBefore($date) {
    $this->joins['changed'] = static::changeJoins('changed');
    $this->conditions['changed_before'] = 'and changed_c.changed <= unix_timestamp(' . $this->sanitizeDate($date) . ")";
    $this->dates['changed_before'] = $date;
    return $this;
  }

  public function changedBeforeLastWeek() {
    $this->joins['changed'] = static::changeJoins('changed');
    $this->conditions['changed_before'] = 'and changed_c.changed <= unix_timestamp(' . static::WEEK_AGO . ")";
    $this->dates['changed_before'] = date('Y-m-d', strtotime('-1 week'));
    return $this;
  }

  public function changedAfter($date) {
    $this->joins['changed'] = static::changeJoins('changed');
    $this->conditions['changed_after'] = 'and changed_c.changed > unix_timestamp(' . $this->sanitizeDate($date) . ")";
    $this->dates['changed_after'] = $date;
    return $this;
  }

  public function changedLastWeek() {
    $this->joins['changed'] = static::changeJoins('changed');
    $this->conditions['changed_after'] = 'and changed_c.changed > unix_timestamp(' . static::WEEK_AGO . ")";
    $this->dates['changed_after'] = date('Y-m-d', strtotime('-1 week'));
    return $this;
  }

  public function fixedLastWeek() {
    $this->conditions['fixed_after'] = static::andC($this->changesNewCondition('status', array(static::FIXED), FALSE, 'fixed') . ' and fixed_c.changed > unix_timestamp(' . static::WEEK_AGO . ")");
    $this->dates['fixed_after'] = date('Y-m-d', strtotime('-1 week'));
    return $this;
  }

  public function fixedAfter($date) {
    $this->conditions['fixed_after'] = static::andC($this->changesNewCondition('status', array(static::FIXED), FALSE, 'fixed') . ' and fixed_c.changed > unix_timestamp(' . $this->sanitizeDate($date) . ")");
    $this->dates['fixed_after'] = $date;
    return $this;
  }

  public function fixedBefore($date) {
    $this->conditions['fixed_before'] = static::andC($this->changesNewCondition('status', array(static::FIXED), FALSE, 'fixed') . ' and fixed_c.changed <= unix_timestamp(' . $this->sanitizeDate($date) . ")");
    $this->dates['fixed_before'] = $date;
    return $this;
  }

  public function movedFromBranch($branch) {
    $branch = static::escapeString($branch);
    $this->conditions['moved_from_branch'] = static::andC($this->changesOldCondition('version', array($branch), FALSE, 'moved_from_branch'));
  }

  public function backportedFrom80xLastWeek() {
    $this->conditions['backported_after'] = static::andC($this->changesOldCondition('version', array('8.0.x-dev'), FALSE, 'backported') . ' and backported_c.changed > unix_timestamp(' . static::WEEK_AGO . ")");
    $this->dates['backported_after'] = date('Y-m-d', strtotime('-1 week'));
    return $this;
  }

  public function backportedFrom80xAfter($date) {
    $this->conditions['backported_after'] = static::andC($this->changesOldCondition('version', array('8.0.x-dev'), FALSE, 'backported') . ' and backported_c.changed > unix_timestamp(' . $this->sanitizeDate($date) . ")");
    $this->dates['backported_after'] = $date;
    return $this;
  }

  public function backportedFrom80xBefore($date) {
    $this->conditions['backported_before'] = static::andC($this->changesOldCondition('version', array('8.0.x-dev'), FALSE, 'backported') . ' and backported_c.changed <= unix_timestamp(' . $this->sanitizeDate($date) . ")");
    $this->dates['backported_before'] = $date;
    return $this;
  }

  public function backportedFrom7xLastWeek() {
    $this->conditions['backported_after'] = static::andC($this->changesOldCondition('version', array('7.x-dev'), FALSE, 'backported') . ' and backported_c.changed > unix_timestamp(' . static::WEEK_AGO . ")");
    $this->dates['backported_after'] = date('Y-m-d', strtotime('-1 week'));
    return $this;
  }

  public function backportedFrom7xAfter($date) {
    $this->conditions['backported_after'] = static::andC($this->changesOldCondition('version', array('7.x-dev'), FALSE, 'backported') . ' and backported_c.changed > unix_timestamp(' . $this->sanitizeDate($date) . ")");
    $this->dates['backported_after'] = $date;
    return $this;
  }

  public function backportedFrom7xBefore($date) {
    $this->conditions['backported_before'] = static::andC($this->changesOldCondition('version', array('7.x-dev'), FALSE, 'backported') . ' and backported_c.changed <= unix_timestamp(' . $this->sanitizeDate($date) . ")");
    $this->dates['backported_before'] = $date;
    return $this;
  }

  public function oldStatus($status_labels) {
    $this->conditions['old_status'] = static::andC($this->changesOldCondition('status', $status_labels));
    return $this;
  }

  public function newStatus($status_labels) {
    $this->conditions['new_status'] = static::andC($this->changesNewCondition('status', $status_labels));
    return $this;
  }

  public function newPriority($priority_labels) {
    $this->conditions['new_status'] = static::andC($this->changesNewCondition('priority', $priority_labels));
    return $this;
  }

  /**
   * Formats and sanitizes a date for use in a query.
   *
   * @param string $date
   *   A valid date string (that will work with strtotime()).
   *
   * @return string
   *   A valid MySQL date (with time code).
   */
  protected function sanitizeDate($date) {
    return "'" . date('Y-m-d', strtotime($date)) . " 00:00:00'";
  }

  /**
   * Sets defaults for what should be queried.
   *
   * By default, we select the node ID and title for open and fixed bugs,
   * tasks, features, and plans against 8.0.x-dev. If specific select fields,
   * projects, versions, issue categories, or issue statuses have been added
   * before this is run, these override the defaults.
   *
   * @see IssueQuery::$select
   * @see IssueQuery::$project
   * @see IssueQuery::version
   * @see IssueQuery::$category
   * @see IssueQuery::$status
   *
   * @todo This method should really only be run as part of the query build
   *   process. Should we add a flag indicating if the defaults have been set?
   *   If any of the value-adding methods are run on the query after this, they
   *   will be added to the defaults rather than overriding the,.
   */
  protected function setDefaults() {
    if (empty($this->select)) {
      $this->selectDistinctNID();
      $this->selectTitle();
    }
    if (empty($this->project)) {
      $this->addCore();
    }
    if (empty($this->version)) {
      $this->addStable();
      $this->addPrep();
      $this->addDev();
    }
    if (empty($this->category)) {
      $this->addBug();
      $this->addTask();
      $this->addPlan();
      $this->addFeature();
    }
    if (empty($this->status)) {
      $this->addOpen();
      $this->addFixed();
    }

  }

  protected static function in($field, array $values, $not_in = FALSE) {
    if (empty($values)) {
      return '';
    }
    $not = $not_in ? 'not' : '';
    $quoted = array();
    foreach ($values as $value) {
      $quoted[] = "'" . $value . "'";
    }
    return "$field $not in (" . implode(', ', $quoted) . ")";
  }

   protected static function like($field, array $values, $not_in = FALSE) {
    if (empty($values)) {
      return '';
    }
    $not = $not_in ? 'not' : '';
    $condition_type = $not_in ? ' and ' : ' or ';
    $conditions = array();
    foreach ($values as $value) {
      $conditions[] = "$field $not like '$value'";
    }
    return ' (' . implode($condition_type, $conditions) . ') ';
  }

  protected static function andC($condition) {
    if (empty($condition)) {
      return '';
    }
    return ' and (' . $condition . ")\n";
  }

  protected static function orC($condition) {
    if (empty($condition)) {
      return '';
    }
    return ' or (' . $condition . ")\n";
  }

  /**
   * Constructs a condition statement for the old value of a changes field.
   *
   * @param $field_label
   *   Field label. This is a shorthand for the name of the field that has been
   *   changed, and should correspond to one of the key names in
   *   IssueQuery::$changeStrings.
   * @param array $value_keys
   *   (optional) Specific values to match for the changed field. These should
   *   correspond to the subkeys of IssueQuery::$changeStrings.
   * @param bool $not_in
   *   (optional) Whether to match values that are not in the list from
   *   $value_keys, instead of values in that list. Defaults to FALSE.
   * @param string $alias
   *   (optional) The table alias prefix to use for the conditions and
   *   for IssueQuery::changeJoins().
   * @return string

   * @see IssueQuery::$changeStrings
   * @see IssueQuery::changeJoins()
   */
  protected function changesOldCondition($field_label, array $value_keys = array(), $not_in = FALSE, $alias = 'changed') {
    return $this->changesCondition($field_label, $value_keys, FALSE, $not_in, $alias);
  }

  /**
   * Constructs a condition statement for the new value of a changes field.
   *
   * @param $field_label
   *   Field label. This is a shorthand for the name of the field that has been
   *   changed, and should correspond to one of the key names in
   *   IssueQuery::$changeStrings.
   * @param array $value_keys
   *   (optional) Specific values to match for the changed field. These should
   *   correspond to the subkeys of IssueQuery::$changeStrings.
   * @param bool $not_in
   *   (optional) Whether to match values that are not in the list from
   *   $value_keys, instead of values in that list. Defaults to FALSE.
   * @param string $alias
   *   (optional) The table alias prefix to use for the conditions and for
   *   IssueQuery::changeJoins().
   * @return string

   * @see IssueQuery::$changeStrings
   * @see IssueQuery::changeJoins()
   */
  protected function changesNewCondition($field_label, array $value_keys = array(), $not_in = FALSE, $alias = 'changed') {
    return $this->changesCondition($field_label, $value_keys, TRUE, $not_in, $alias);
  }

  /**
   * Constructs a condition statement for the changes field.
   *
   * @param $field_label
   *   Field label. This is a shorthand for the name of the field that has been
   *   changed, and should correspond to one of the key names in
   *   IssueQuery::$changeStrings.
   * @param array $value_keys
   *   (optional) Specific values to match for the changed field. These should
   *   correspond to the subkeys of IssueQuery::$changeStrings.
   * @param bool $new
   *   (optional) Whether to match the new value of the field (versus the old
   *   one). Defaults to TRUE.
   * @param bool $not_in
   *   (optional) Whether to match values that are not in the list from
   *   $value_keys, instead of values in that list. Defaults to FALSE.
   * @param string $alias
   *   (optional) The table alias prefix to use for the conditions and for
   *   IssueQuery::changeJoins().
   * @return string
   *
   * @see IssueQuery::$changeStrings
   * @see IssueQuery::changeJoins()
   */
  protected function changesCondition($field_label, array $value_keys = array(), $new = TRUE, $not_in = FALSE, $alias = 'changed') {
    $this->joins[$alias] = static::changeJoins($alias);
    $alias = static::escapeTableName($alias);
    if (empty($value_keys)) {
      return '';
    }
    if ($field_label == 'project') {
      $field = 'field_project';
    }
    elseif ($field_label == 'tags') {
      $field = 'taxonomy_vocabulary_9';
    }
    else {
      $field = 'field_issue_' . $field_label;
    }
    $values = array();
    foreach ($value_keys as $value_key) {
      $values[] = $this->changeStrings[$field_label][$value_key];
    }
    $condition = " ({$alias}_fc.field_issue_changes_field_name = '$field' ";
    $condition .= ' and ';
    if ($field_label == 'tags') {
      $condition .= static::like($new ? "{$alias}_fc.field_issue_changes_new_value" : "{$alias}_fc.field_issue_changes_old_value", $values, $not_in);
    }
    else {
      $condition .= static::in($new ? "{$alias}_fc.field_issue_changes_new_value" : "{$alias}_fc.field_issue_changes_old_value", $values, $not_in);
    }
    $condition .= ")";
    return $condition;
  }

  /**
   * Returns the base joins for the query or a subquery.
   *
   * By default, we join to the project, version, priority, category, and
   * status fields. Additional joins are added in IssueQuery::$joins.
   *
   * @return string
   *   String containing a FROM statement and inner joins to the primary issue
   *   fields.
   *
   * @see IssueQuery::$joins
   */
  protected static function baseJoins() {
    return <<<EOD

from node n

inner join field_data_field_project project on project.entity_id = n.nid
inner join field_data_field_issue_version version on version.entity_id = n.nid
inner join field_data_field_issue_priority p on p.entity_id = n.nid
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid

EOD;
  }

  protected static function changeJoins($alias = 'changed') {
    $alias = static::escapeTableName($alias);
    return <<<EOD
inner join field_data_field_issue_changes {$alias}_fc on {$alias}_fc.field_issue_changes_nid = n.nid
inner join comment {$alias}_c on {$alias}_c.cid = {$alias}_fc.entity_id

EOD;
  }

  protected function commenterJoins(array $users, $alias = 'octo') {
    $uid_string = implode(', ', $this->getUids($users));
    return <<<EOD
inner join comment {$alias}_c on {$alias}_c.nid = n.nid and {$alias}_c.uid in ({$uid_string})
left join field_data_comment_body {$alias}_cb on {$alias}_c.cid = {$alias}_cb.entity_id

EOD;
  }

  protected function getUids(array $users) {
    $query_uids = array();
    foreach ($users as $user) {
      $query_uids[] = $this->uids[$user];
    }
    return $query_uids;
  }

  protected function octoJoins(array $users = array()) {
    if (empty($users)) {
      $users = $this->octo;
    }
    $uid_string = implode(', ', $this->getUids($users));
    return $this->commenterJoins($users, 'octo') . <<<EOD
left join field_data_field_issue_files octo_ff on octo_ff.entity_id = n.nid
left join file_managed octo_files on octo_files.fid = octo_ff.field_issue_files_fid  and octo_files.uid in ({$uid_string})

EOD;
  }

  protected function buildBaseConditions() {
    return static::andC(static::in('project.field_project_target_id', $this->project))
      . static::andC(static::in('version.field_issue_version_value', $this->version))
      . static::andC(static::in('p.field_issue_priority_value', $this->priority))
      . static::andC(static::in('c.field_issue_category_value', $this->category))
      . static::andC(static::in('s.field_issue_status_value', $this->status))
      ;

  }

  /**
   * Builds the query based on its current state.
   *
   * How the query is built:
   * 1. If there is a static query set, this is returned instead of building a
   *    query.
   * 2. Default values for the selected fields, project, version, priority,
   *    category, and status are merged in with any that have been set already.
   * 3. The select statement is composed based on IssueQuery::$select.
   * 4. The node base table and default joins are added to the main query. See
   *    IssueQuery::baseJoins().
   * 5. Additional joins defined in IssueQuery::$joins are added.
   * 6. If IssueQuery::$commentData is set, a subquery is added for issue
   *    commenters.
   * 7. If IssueQuery::$patchData iss set, a subquery is added for patches
   *    posted on the issue.
   * 8. Only published nodes are ever selected, to ensure we never leak any
   *    unpublished nodes for private issues.
   * 9. Any base conditions are added (for the project, version, priority,
   *    category, and status, as joined in step 4).
   * 10. A subquery is added to exclude results without a certain taxonomy term,
   *    based on any values in IssueQuery::$withoutTaxonomyTerm.
   * 11. If IssueQuery::$movedIntoQueue is set, conditions for the changes
   *    field are added to only include results that were previously not in the
   *    given "queue" (based on the base conditions of project, version,
   *    priority, category, and status). See IssueQuery::changesCondition().
   * 12. Any additional conditions defined in IssueQuery::$conditions are
   *    added. These conditions may use any of:
   *    - the base joins
   *    - the joins added from IssueQuery::$joins
   *    - node_comments for the comment data
   *    - node_files for the patch data
   * 13. Order by conditions are added from IssueQuery::$orderBy, using the
   *    order in which they were added to the query during the definition.
   * 14. Limits the query by IssueQuery::$limit.
   *
   * @param bool $set_defaults
   *
   * @return string
   *   The assembled query.
   *
   * @see IssueQuery::$staticQuery
   * @see IssueQuery::$select
   * @see IssueQuery::$joins
   * @see IssueQuery::$commentData
   * @see IssueQuery::$patchData
   * @see IssueQuery::$withoutTaxonomyTerm
   * @see IssueQuery::$movedIntoQueue
   * @see IssueQuery::$conditions
   * @see IssueQuery::$orderBy
   */
  public function buildQuery($set_defaults = TRUE) {
    if ($this->staticQuery) {
      return $this->staticQuery;
    }
    if ($set_defaults) {
      $this->setDefaults();
    }

    // Add fields to select and base joins.
    $query = "\n\n";
    $query .= "select " . implode(",\n", $this->select) . static::baseJoins() . "\n\n";

    // Add additional joins.
    $query .= implode("\n\n", $this->joins) . "\n\n";

    // Add join for comment subqueries.
    if ($this->commentData) {
      $query .= "left join (\n"
        . "select n.nid, count(cm.cid) as comment_count, max(cm.changed) as recent_comment_date, replace(group_concat(distinct(u.name)), ',', ', ') as commenters \n"
        . static::baseJoins()
        . "inner join comment cm on cm.nid = n.nid \n"
        . "inner join users u on u.uid = cm.uid \n"
        // @todo taxonomy terms here too.
        . $this->buildBaseConditions()
        . "group by n.nid \n"
        . ") node_comments on node_comments.nid = n.nid\n"
        ;
    }

    // Add join for patch subquery.
    if ($this->patchData) {
      $query .= "left join (\n"
        . 'select n.nid, max(files.timestamp) as last_patch_date'
        . static::baseJoins()
        . "inner join field_data_field_issue_files i_files on i_files.entity_id = n.nid \n"
        . "inner join file_managed files on i_files.field_issue_files_fid = files.fid \n"
        // @todo taxonomy terms.
        . $this->buildBaseConditions()
        . "and files.filename LIKE '%.patch' \n"
        . "group by n.nid \n"
        . ") node_files on node_files.nid = n.nid\n"
        ;
    }

    // Always select only published nodes.
    $query .= "where n.status = 1\n";

    // Add base conditions.
    $query .= $this->buildBaseConditions() . "\n";

    // Add negated taxonomy conditions (which require subqueries).
    foreach ($this->withoutTaxonomyTerm as $tid) {
      $query .= " and n.nid not in (select n.nid "
      . static::baseJoins()
      . static::taxonomyTermJoin($tid) . "\n"
      . $this->buildBaseConditions()
      . ")\n";
      ;
    }

    // Add old statuses for issues moved into the queue.
    if ($this->movedIntoQueue) {
      $query .= " and (\n";
      $query .= $this->changesOldCondition('project', $this->project, TRUE);
      $query .= static::orC($this->changesOldCondition('version', $this->version, TRUE));
      $query .= static::orC($this->changesOldCondition('category', $this->category, TRUE));
      $query .= static::orC($this->changesOldCondition('priority', $this->priority, TRUE));
      $query .= static::orC($this->changesOldCondition('status', array(
        static::ACTIVE,
        static::NW,
        static::NR,
        static::RTBC,
        static::POSTPONED,
        static::FIXED,
        static::CLOSED_FIXED
      ), TRUE));
      // @todo moved into taxonomy term queues also.
      $query .= ")\n\n";
    }

    // Add static conditions.
    $query .= implode("\n\n", $this->conditions) . "\n\n";

    if (!empty($this->groupBy)) {
      $query .= "group by " . implode(', ', $this->groupBy);
      $query .= "\n";
    }


    $query .= "order by " . implode(', ', $this->orderBy);
    $query .= "\n";

    if (!empty($this->limit)) {
      $query .= 'limit ' . $this->limit;
      $query .= "\n";
    }

    $query .= ";\n\n";

    return $query;
  }

}
