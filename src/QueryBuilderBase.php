<?php

namespace Drupal\core_metrics;

class QueryBuilderBase {

  /**
   * An assoiciative array of issue queries, keyed by the query name.
   *
   * @var IssueQuery[]
   */
  protected $queries = array();

  /**
   * Returns the array of queries.
   *
   * @return
   *  The query list, keyed by query name.
   */
  public function getQueries() {
    return $this->queries;
  }

}