<?php

namespace Drupal\core_metrics\core_release;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {
    // Approximate the date of the last patch release.
    //$last_release_date = date('Y-m-d', strtotime('first wednesday of this month', strtotime('now -1 month')));
    $last_release_date = '2020-12-02';

    // This release number.
    // @todo Integrate this somehow with the drupal_core_release scripts.
    $version = '9.2.0';

    /**
     * We no longer list fixed criticals nor issues with string changes.
    $this->queries['fixed_criticals'] = new IssueQuery();
    $this->queries['fixed_criticals']
//      ->addStable()
      ->addPrep()
      ->addFixed()
      ->addCritical()
      ->fixedAfter($last_release_date)
      ;

    $this->queries['string_change'] = new IssueQuery();
    $this->queries['string_change']
      ->addStable()
      ->addPrep()
      ->addFixed()
      ->addStringChange($version)
      ;
    **/

    // Fixed issues tagged for the release notes.
    $this->queries['fixed_rn_mention'] = new IssueQuery();
    $this->queries['fixed_rn_mention']
      ->addFixed()
      ->addReleaseNotes($version)
      ;
    // Open issues tagged for mention in the release, regardless of which
    // branch they are filed against.
    $this->queries['open_rn_mention'] = new IssueQuery();
    $this->queries['open_rn_mention']
      ->addOpen()
      ->addReleaseNotes($version)
      ;
  }

}
