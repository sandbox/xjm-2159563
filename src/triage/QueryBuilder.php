<?php

namespace Drupal\core_metrics\triage;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {

    $this->queries['untriaged_criticals'] = new IssueQuery();
    $this->queries['untriaged_criticals']
      ->selectDistinctNID()
      ->selectTitle()
      ->selectComponent()
      ->selectAge()
      ->selectUpdated()
      ->selectPatchUpdated()
      ->addOpen()
      ->addCritical()
      ->addNotTriaged()
      ->orderBy(array('c.field_issue_category_value', 'last_patch_date', 'n.nid'))
      ;

    $this->queries['triaged_without_maintainer'] = new IssueQuery();
    $this->queries['triaged_without_maintainer']
      ->addOpen()
      ->addCritical()
      ->addTriaged()
      ->notTriagedByMaintainers()
      ;

    $this->queries['untriaged_major_bugs'] = new IssueQuery();
    $this->queries['untriaged_major_bugs']
      ->selectDistinctNID()
      ->selectTitle()
      ->selectComponent()
      ->selectAge()
      ->selectUpdated()
      ->selectPatchUpdated()
      ->addOpen()
      ->addMajor()
      ->addBug()
      ->addMajorNotTriaged()
      ->orderBy(array('last_patch_date', 'n.nid'))
      ;

    $this->queries['untriaged_current_state'] = new IssueQuery();
    $this->queries['untriaged_current_state']
      ->selectDistinctNID()
      ->selectTitle()
      ->selectComponent()
      ->selectAge()
      ->selectUpdated()
      ->selectPatchUpdated()
      ->addOpen()
      ->addMajor()
      ->addMajorNotTriaged()
      ->addCurrentState()
      ->orderBy(array('last_patch_date', 'n.nid'))
      ;

    $this->queries['base_major_bugs'] = new IssueQuery();
    $this->queries['base_major_bugs']
      ->selectDistinctNID()
      ->selectTitle()
      ->selectAge()
      ->selectUpdated()
      ->selectPatchUpdated()
      ->addOpen()
      ->addMajor()
      ->addBug()
      ->addMajorNotTriaged()
      ->addBase()
      ->orderBy(array('last_patch_date', 'n.nid'))
      ;

    $this->queries['vdc_major_bugs'] = new IssueQuery();
    $this->queries['vdc_major_bugs']
      ->selectDistinctNID()
      ->selectTitle()
      ->selectComponent()
      ->selectAge()
      ->selectUpdated()
      ->selectPatchUpdated()
      ->addOpen()
      ->addMajor()
      ->addBug()
      ->addMajorNotTriaged()
      ->addVDC()
      ->orderBy(array('last_patch_date', 'n.nid'))
      ;

    $this->queries['theme_major_bugs'] = new IssueQuery();
    $this->queries['theme_major_bugs']
      ->selectDistinctNID()
      ->selectTitle()
      ->selectComponent()
      ->selectAge()
      ->selectUpdated()
      ->selectPatchUpdated()
      ->addOpen()
      ->addMajor()
      ->addBug()
      ->addMajorNotTriaged()
      ->addTheme()
      ->orderBy(array('last_patch_date', 'n.nid'))
      ;

    $this->queries['rest_majors'] = new IssueQuery();
    $this->queries['rest_majors']
      ->selectDistinctNID()
      ->selectTitle()
      ->selectComponent()
      ->selectAge()
      ->selectUpdated()
      ->selectPatchUpdated()
      ->addOpen()
      ->addMajor()
      ->addMajorNotTriaged()
      ->addREST()
      ->orderBy(array('last_patch_date', 'n.nid'))
      ;

    $this->queries['api_first_blocker_majors'] = new IssueQuery();
    $this->queries['api_first_blocker_majors']
      ->selectDistinctNID()
      ->selectTitle()
      ->selectComponent()
      ->selectAge()
      ->selectUpdated()
      ->selectPatchUpdated()
      ->addOpen()
      ->addMajor()
      ->addMajorNotTriaged()
      ->addAPIFirstBlocker()
      ->orderBy(array('last_patch_date', 'n.nid'))
      ;

    $this->queries['entity_major_bugs'] = new IssueQuery();
    $this->queries['entity_major_bugs']
      ->selectDistinctNID()
      ->selectTitle()
      ->selectComponent()
      ->selectAge()
      ->selectUpdated()
      ->selectPatchUpdated()
      ->addOpen()
      ->addMajor()
      ->addBug()
      ->addMajorNotTriaged()
      ->addEntity()
      ->orderBy(array('last_patch_date', 'n.nid'))
      ;

  }

}
