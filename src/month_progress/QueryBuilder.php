<?php

namespace Drupal\core_metrics\month_progress;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {
    $date = '2014-10-30';

    $this->queries['fixed_criticals'] = new IssueQuery();
    $this->queries['fixed_criticals']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->fixedAfter($date)
      ;
    $this->queries['new_criticals'] = new IssueQuery();
    $this->queries['new_criticals']
      ->selectCount()
      ->addCritical()
      ->createdAfter($date)
      ;
    $this->queries['promoted_criticals'] = new IssueQuery();
    $this->queries['promoted_criticals']
      ->selectCount()
      ->addCritical()
      ->movedIntoQueue()
      ->createdBefore($date)
      ->changedAfter($date)
      ->fixedAfter($date)
      ;
    $this->queries['reopened_criticals'] = new IssueQuery();
    $this->queries['reopened_criticals']
      ->selectCount()
      ->addOpen()
      // @todo This does not guarantee it was critical before it was reopened,
      // just that it is critical now and was once fixed.
      ->oldStatus(array(IssueQuery::FIXED, IssueQuery::CLOSED_FIXED))
      ->changedAfter($date)
      ->addCritical()
      ;
    $this->queries['open_criticals'] = new IssueQuery();
    $this->queries['open_criticals']
      ->selectCount()
      ->addOpen()
      ->addCritical()
      ;
    $this->queries['open_d8up'] = new IssueQuery();
    $this->queries['open_d8up']
      ->selectCount()
      ->addOpen()
      ->addCritical()
      ->addD8up()
      ;
    $this->queries['fixed_d8up'] = new IssueQuery();
    $this->queries['fixed_d8up']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->addD8up()
      ->fixedAfter($date)
      ;
    $this->queries['total_fixed_d8up'] = new IssueQuery();
    $this->queries['total_fixed_d8up']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->addD8up()
      ->fixedAfter('2014-10-01')
      ;
  }

}
