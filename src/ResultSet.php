<?php

namespace Drupal\core_metrics;

/**
 * Defines a result set from CSV file output of Drupal.org DB query results.
 */
class ResultSet {

  /**
   * The subdirectory for the result set.
   *
   * @var string
   */
  protected $set_dir;

  /**
   * The directory containing CSV query results.
   *
   * @var string
   */
  protected $csv_dir;

  /**
   * The directory to write ouptut to, if any.
   *
   * @var string
   */
  protected $write_dir;

  /**
   * A nested array of CSV rows and cells, keyed by result (file) name.
   *
   * @var array
   */
  protected $results = array();

  /**
   * An array of header rows for the results.
   *
   * @var array
   */
  protected $headers = array();

  /**
   * An array of loaded result files.
   *
   * @var array
   */
  protected $files = array();

  /**
   * Constructs a new ResultSet.
   */
  public function __construct($set) {
    $this->setDir = __DIR__ . '/../' . $set;
    $this->csvDir = $this->setDir . '/' . 'csv';
    $this->loadFiles();
    $this->explodeResults();
  }

  /**
   * Loads all valid result files.
   */
  protected function loadFiles() {
    $dir = scandir($this->csvDir);
    foreach ($dir as $filename) {
      if ($result_name = strstr($filename, '.txt.out.csv', TRUE)) {
          $this->files[$result_name] = file($this->csvDir . '/' . $filename, FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
      }
    }
  }

  /**
   * Converts loaded files into a nested array of results.
   */
  protected function explodeResults() {
    foreach ($this->files as $result_name => $result) {
      foreach ($result as $i_line => $line) {
        $this->results[$result_name][$i_line] = explode('","', $line);
        foreach ($this->results[$result_name][$i_line] as $i_cell => $cell) {
          // Remove quotation marks and whitespace.
          $this->results[$result_name][$i_line][$i_cell] = trim(str_replace('"', '', $cell));
        }
      }
      // Store and remove the header row.
      if (!empty($this->results[$result_name])) {
        $this->headers[$result_name] = $this->results[$result_name][0];
        unset($this->results[$result_name][0]);
      }
    }
  }

  /**
   * Returns a particular result.
   *
   * @param string $result_name
   *   The key for the result set (from the filename of the query).
   *
   * @return array
   *   A nested array of the rows and cells for the result.
   */
  public function getResult($result_name) {
    if (!empty($this->results[$result_name])) {
      return $this->results[$result_name];
    }
    // If the result is empty, return an empty array.
    return array();
  }

  /**
   * Returns values for a given column of the result.
   *
   * @param string $result_name
   *   The key for the result set (from the filename of the query).
   * @param string $column_name
   *   The column name for the column (from the query).
   *
   * @return array
   *   A nested array of the rows and cells for the result.
   */
  public function getColumn($result_name, $column_name) {
    $column_index = array_search($column_name, $this->getHeader($result_name));
    if ($column_index === FALSE) {
      throw new \Exception("$column_name not found in $result_name");
    }
    $column = array();
    foreach ($this->results[$result_name] as $row) {
      $column[] = $row[$column_index];
    }
    return $column;
  }

  /**
   * Returns the first row of a result.
   *
   * Useful for a query that returns a single row.
   *
   * @param string $result_name
   *   The key for the result set (from the filename of the query).
   *
   * @return string
   *   The value in the first cell of the first non-header row for the result.
   */
  public function getFirstRow($result_name) {
    return $this->results[$result_name][1];
  }

  /**
   * Returns the first field from the first row of a result.
   *
   * Useful for a query that returns a single field, e.g. a count query.
   *
   * @param string $result_name
   *   The key for the result set (from the filename of the query).
   *
   * @return string
   *   The value in the first cell of the first non-header row for the result.
   */
  public function getFirstCell($result_name) {
    return $this->results[$result_name][1][0];
  }

  /**
   * Returns a particular result's header row.
   *
   * @param string $result_name
   *   The key for the result set (from the filename of the query).
   *
   * @return array
   *   An array containing the cells of the header row for the result.
   */
  public function getHeader($result_name) {
    return $this->headers[$result_name];
  }

}
