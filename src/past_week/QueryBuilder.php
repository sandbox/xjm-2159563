<?php

namespace Drupal\core_metrics\past_week;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  protected $endDate = '2014-12-22';

  protected $startDate;

  public function __construct() {
    $this->startDate = date('Y-m-d', strtotime('-1 week', strtotime($this->endDate)));

    // @todo: This is a fixed string at the moment.
    $this->queries['dbdate'] = new IssueQuery();
    $this->queries['dbdate']->setStaticQuery("select '" . $this->startDate . " 00:00:00' as start_date, '" . $this->endDate . " 00:00:00' as end_date;")
      ;
    // @todo: This is being included from a file at the moment.
    $this->queries['demoted_criticals'] = new IssueQuery();
    $this->queries['demoted_criticals']
      ->setStaticQuery(file_get_contents(__DIR__ . '/../../past_week/demoted_criticals.txt'))
      ;
    $this->queries['fixed_all'] = new IssueQuery();
    $this->queries['fixed_all']
      ->selectCount()
      ->addFixed()
      ->fixedBefore($this->endDate)
      ->fixedAfter($this->startDate)
      ;
    $this->queries['fixed_criticals'] = new IssueQuery();
    $this->queries['fixed_criticals']
      ->addFixed()
      ->addCritical()
      ->fixedBefore($this->endDate)
      ->fixedAfter($this->startDate)
      ;
    $this->queries['new_fixed_criticals'] = new IssueQuery();
    $this->queries['new_fixed_criticals']
      ->addFixed()
      ->addCritical()
      ->createdBefore($this->endDate)
      ->createdAfter($this->startDate)
      ->fixedBefore($this->endDate)
      ->fixedAfter($this->startDate)
      ;
    $this->queries['new_unfixed_criticals'] = new IssueQuery();
    $this->queries['new_unfixed_criticals']
      ->addOpen()
      ->addCritical()
      ->createdBefore($this->endDate)
      ->createdAfter($this->startDate)
      ;
    $this->queries['octo_all'] = new IssueQuery();
    $this->queries['octo_all']
      ->selectCount()
      ->addFixed()
      ->addOCTO()
      ->fixedBefore($this->endDate)
      ->fixedAfter($this->startDate)
      ;
    $this->queries['octo_criticals'] = new IssueQuery();
    $this->queries['octo_criticals']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->addOCTO()
      ->fixedBefore($this->endDate)
      ->fixedAfter($this->startDate)
      ;
    // @todo These are queries for the current date.
/*
    $this->queries['open_criticals'] = new IssueQuery();
    $this->queries['open_criticals']
      ->selectCount()
      ->addOpen()
      ->addCritical()
      ;
    $this->queries['open_d8up'] = new IssueQuery();
    $this->queries['open_d8up']
      ->selectCount()
      ->addOpen()
      ->addCritical()
      ->addD8up()
      ;
    $this->queries['total_criticals'] = new IssueQuery();
    $this->queries['total_criticals']
      ->selectCount()
      ->addCritical()
      ;
    $this->queries['total_fixed_d8up'] = new IssueQuery();
    $this->queries['total_fixed_d8up']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->addD8up()
      ->fixedAfter('2014-10-01')
      ;
*/
    $this->queries['fixed_d8up'] = new IssueQuery();
    $this->queries['fixed_d8up']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->addD8up()
      ->fixedBefore($this->endDate)
      ->fixedAfter($this->startDate)
      ;
    $this->queries['promoted_fixed_criticals'] = new IssueQuery();
    $this->queries['promoted_fixed_criticals']
      ->addFixed()
      ->addCritical()
      ->movedIntoQueue()
      ->changedBefore($this->endDate)
      ->changedAfter($this->startDate)
      ->createdBefore($this->startDate)
      ->fixedBefore($this->endDate)
      ->fixedAfter($this->startDate)
      ;
    $this->queries['promoted_unfixed_criticals'] = new IssueQuery();
    $this->queries['promoted_unfixed_criticals']
      ->addOpen()
      ->addCritical()
      ->movedIntoQueue()
      ->changedBefore($this->endDate)
      ->changedAfter($this->startDate)
      ->createdBefore($this->startDate)
      ;
    $this->queries['reopened_criticals'] = new IssueQuery();
    $this->queries['reopened_criticals']
      ->addOpen()
      // @todo This does not guarantee it was critical before it was reopened,
      // just that it is critical now and was once fixed.
      ->oldStatus(array(IssueQuery::FIXED, IssueQuery::CLOSED_FIXED))
      ->changedBefore($this->endDate)
      ->changedAfter($this->startDate)
      ->addCritical()
      ;
  }

}
