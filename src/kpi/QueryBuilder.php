<?php

namespace Drupal\core_metrics\kpi;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {
    $date = '2015-07-01';

    $this->queries['fixed_criticals'] = new IssueQuery();
    $this->queries['fixed_criticals']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->fixedAfter($date)
      ;
    $this->queries['octo_criticals'] = new IssueQuery();
    $this->queries['octo_criticals']
      ->selectCount()
      ->addFixed()
      ->addCritical()
      ->addOCTO()
      ->fixedAfter($date)
      ;
  }

}
