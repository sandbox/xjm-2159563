<?php

namespace Drupal\core_metrics\signoffs;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {

    $start_date = '2017-01-01';
    $end_date = '2017-03-02';

    $this->queries['untag'] = new IssueQuery();
    $this->queries['untag']
      ->selectDistinctNID()
      ->maintainerUntag('fm_review', array('effulgentsia'))
      ->changedAfter($start_date)
      ->changedBefore($end_date)
      ;

    $this->queries['untag_with_comment'] = new IssueQuery();
    $this->queries['untag_with_comment']
      ->selectDistinctNID()
      ->untag('fm_review')
      ->changedAfter($start_date)
      ->changedBefore($end_date)
      ->addCommenterTimeframe(array('effulgentsia'), $start_date, $end_date);
      ;

    $this->queries['tagged_with_comment'] = new IssueQuery();
    $this->queries['tagged_with_comment']
      ->selectDistinctNID()
      ->addTag('fm_review')
      ->addCommenterTimeframe(array('effulgentsia'), $start_date, $end_date);
      ;

  }

}
