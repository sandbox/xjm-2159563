<?php

namespace Drupal\core_metrics\rtbc;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {

    $start_date = '2016-04-20';

    $this->queries['rtbc_bot_fail'] = new IssueQuery();
    $this->queries['rtbc_bot_fail']
      ->selectNID()
      ->selectChangedDate()
      ->addRTBC()
      ->maintainerUnRTBC(array('system_message'))
      ;

    $this->queries['rtbc_last_rtbc'] = new IssueQuery();
    $this->queries['rtbc_last_rtbc']
      ->selectNID()
      ->selectLastRTBC()
      ->addRTBC()
      ;

    $this->queries['fixed'] = new IssueQuery();
    $this->queries['fixed']
      ->selectDistinctNID()
      ->selectFixedDate()
      ->addFixed()
      ->fixedAfter($start_date)
      ;

    $this->queries['rtbced'] = new IssueQuery();
    $this->queries['rtbced']
      ->selectNID()
      ->selectRTBCed()
      ->changedAfter($start_date)
      ;

    $this->queries['un_rtbced'] = new IssueQuery();
    $this->queries['un_rtbced']
      ->selectNID()
      ->selectUnRTBCed()
      ->changedAfter($start_date)
      ;

    // Test fails.

    // Maintainer feedback.


    // The combo of two changes joins here makes the query take too long.
    /*
    $this->queries['fixed_last_rtbc'] = new IssueQuery();
    $this->queries['fixed_last_rtbc']
      ->selectNID()
      ->selectLastRTBC()
      ->selectFixedDate()
      ->addFixed()
      ;
    */

  }

}
