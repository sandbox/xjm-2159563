<?php

namespace Drupal\core_metrics\components;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {

    $this->queries['untriaged_criticals'] = new IssueQuery();
    $this->queries['untriaged_criticals']
      ->selectCount()
      ->selectComponent()
      ->addOpen()
      ->addMajor()
      ->addBug()
      ->groupBy(array('component'))
      ->orderBy(array('node_count'))
      ;

  }

}
