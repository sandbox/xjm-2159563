<?php

namespace Drupal\core_metrics\maintainers;

use \Drupal\core_metrics\QueryBuilderBase;
use \Drupal\core_metrics\IssueQuery;

class QueryBuilder extends QueryBuilderBase {

  public function __construct() {

    $months = array('2017-08-01', '2017-09-01', '2017-10-01', '2017-11-01', '2017-12-01');

    foreach ($months as $month) {
      $month_key = str_replace('-', '_', $month);
      $end_date = date('Y-m-d', strtotime("$month +1 month"));
      $this->queries['un_rtbc_alexpott_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_alexpott_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('alexpott'))
        ->changedAfter($month)
        ->changedBefore($end_date);
      $this->queries['un_rtbc_webchick_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_webchick_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('webchick'))
        ->changedAfter($month)
        ->changedBefore($end_date);
      $this->queries['un_rtbc_catch_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_catch_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('catch'))
        ->changedAfter($month)
        ->changedBefore($end_date);
      $this->queries['un_rtbc_xjm_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_xjm_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('xjm'))
        ->changedAfter($month)
        ->changedBefore($end_date);
      $this->queries['un_rtbc_larowlan_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_larowlan_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('larowlan'))
        ->changedAfter($month)
        ->changedBefore($end_date);
      $this->queries['un_rtbc_lauriii_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_lauriii_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('lauriii'))
        ->changedAfter($month)
        ->changedBefore($end_date);
      $this->queries['un_rtbc_effulgentsia_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_effulgentsia_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('effulgentsia'))
        ->changedAfter($month)
        ->changedBefore($end_date);
      $this->queries['un_rtbc_plach_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_plach_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('effulgentsia'))
        ->changedAfter($month)
        ->changedBefore($end_date);
      $this->queries['un_rtbc_cottser_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_cottser_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('cottser'))
        ->changedAfter($month)
        ->changedBefore($end_date);
      $this->queries['un_rtbc_cilefen_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_cilefen_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('cilefen'))
        ->changedAfter($month)
        ->changedBefore($end_date);
      $this->queries['un_rtbc_dries_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_dries_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('dries'))
        ->changedAfter($month)
        ->changedBefore($end_date);
      $this->queries['un_rtbc_gabor_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_gabor_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('gabor'))
        ->changedAfter($month)
        ->changedBefore($end_date);
      $this->queries['un_rtbc_yoroy_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_yoroy_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('yoroy'))
        ->changedAfter($month)
        ->changedBefore($end_date);
      $this->queries['un_rtbc_wim_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_wim_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('wim'))
        ->changedAfter($month)
        ->changedBefore($end_date);
      $this->queries['un_rtbc_timplunkett_' . $month_key] = new IssueQuery();
      $this->queries['un_rtbc_timplunkett_' . $month_key]
        ->selectCount()
        ->maintainerUnRTBC(array('timplunkett'))
        ->changedAfter($month)
        ->changedBefore($end_date);
    }
  }

}
