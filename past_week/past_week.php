<?php

include_once(__DIR__ . '/../src/ResultSet.php');

use Drupal\core_metrics\ResultSet;

$r = new ResultSet('past_week');

$dates = $r->getFirstRow('dbdate');
foreach ($dates as $i => $date) {
  $dates[$i] = date('Y-m-d', strtotime($date));
}

$new_fixed = $r->getResult('new_fixed_criticals');
$promoted_fixed = $r->getResult('promoted_fixed_criticals');
$new_unfixed = $r->getResult('new_unfixed_criticals');
$promoted_unfixed = $r->getResult('promoted_unfixed_criticals');
$demoted = $r->getResult('demoted_criticals');
$reopened = $r->getResult('reopened_criticals');

$total_d8up_count = $r->getFirstCell('total_fixed_d8up') + $r->getFirstCell('open_d8up');
$fixed_count = sizeof($r->getResult('fixed_criticals'));
$demoted_count = sizeof($demoted);
$total_new_count = sizeof($new_fixed) + sizeof($promoted_fixed) + sizeof($new_unfixed) + sizeof($promoted_unfixed) + sizeof($reopened);
$net = $total_new_count - $fixed_count - $demoted_count;
$fewer_more = ($net <= 0) ? 'fewer' : 'more';
$net = abs($net);
$net_s = ((int) $net === 1) ? '' : 's';

print "

Drupal 8 weekly progress report

========================

Detailed Drupal 8 progress between {$dates[0]} and {$dates[1]}:

{$r->getFirstCell('fixed_all')} fixed issues ({$r->getFirstCell('octo_all')} with OCTO contribution)
$fixed_count fixed critical issues ({$r->getFirstCell('octo_criticals')} with OCTO contribution)
$demoted_count deprioritized (previously critical) issues
$total_new_count new critical issues

New critical issues (unresolved as of {$dates[1]}):

";

foreach (array($new_unfixed, $promoted_unfixed) as $node_array) {
  foreach ($node_array as $node) {
    print "https://www.drupal.org/node/{$node[0]} {$node[1]}\n";
  }
}

print "
Explanations:

- 'Critical issues' are open Drupal 8 issues with the 'critical' priority, all of which must be resolved before a creating a Drupal 8 release candidate. For more information on Drupal 8 progress, see: https://www.drupal.org/drupal-8.0/get-involved and https://groups.drupal.org/core/updates

- 'Upgrade path blockers' are critical issues that block a Drupal 8 beta-to-beta upgrade path (a key milestone for early adopters).

- 'New' critical issues include both newly discovered bugs and known issues that have had their priority increased.

- 'Deprioritized' issues include issues that were previously considered critical but have been moved or reassessed.

- 'OCTO contribution' means that at least one of our five full-time Drupal 8 engineers contributed in a significant way to resolving the issue (with code review, project management, or a patch). In most cases, OCTO engineers collaborate with non-Acquian core contributors to resolve these issues efficiently.






Drupal 8 progress between {$dates[0]} and {$dates[1]}:

We have $net $fewer_more critical Drupal 8 issue$net_s between {$dates[0]} and {$dates[1]}.

$fixed_count fixed critical issues
$demoted_count deprioritized  (previously critical) issues
$total_new_count new critical issues

New critical issues (unresolved as of {$dates[1]}):

";

foreach (array($new_unfixed, $promoted_unfixed) as $node_array) {
  foreach ($node_array as $node) {
    print "https://www.drupal.org/node/{$node[0]} {$node[1]}\n";
  }
}

print "
Deprioritized criticals:

";

foreach ($demoted as $node) {
  print "https://www.drupal.org/node/{$node[0]} {$node[1]}\n";
}

print "

Reopened criticals:

";


foreach ($reopened as $node) {
  print "https://www.drupal.org/node/{$node[0]} {$node[1]}\n";
}

print "


";
