

select distinct n.nid,
replace(n.title, '"', '') as title
from node n

inner join field_data_field_project project on project.entity_id = n.nid
inner join field_data_field_issue_version version on version.entity_id = n.nid
inner join field_data_field_issue_priority p on p.entity_id = n.nid
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid


inner join field_data_field_issue_changes change_fc on change_fc.field_issue_changes_nid = n.nid
inner join comment change_c on change_c.cid = change_fc.entity_id

where n.status = 1
 and (project.field_project_target_id  in ('3060'))
 and (version.field_issue_version_value  in ('8.0.x-dev'))
 and (p.field_issue_priority_value  in ('400'))
 and (c.field_issue_category_value  in ('1', '2'))
 and (s.field_issue_status_value  in ('1', '13', '8', '14', '4'))

 and (
 (change_fc.field_issue_changes_field_name = 'field_project'  and change_fc.field_issue_changes_old_value not in ('a:1:{i:0;a:1:{s:9:"target_id";s:4:"3060";}}')) or ( (change_fc.field_issue_changes_field_name = 'field_issue_version'  and change_fc.field_issue_changes_old_value not in ('a:1:{i:0;a:1:{s:5:"value";s:9:"8.0.x-dev";}}')))
 or ( (change_fc.field_issue_changes_field_name = 'field_issue_category'  and change_fc.field_issue_changes_old_value not in ('a:1:{i:0;a:1:{s:5:"value";s:3:"bug";}}', 'a:1:{i:0;a:1:{s:5:"value";s:4:"task";}}')))
 or ( (change_fc.field_issue_changes_field_name = 'field_issue_priority'  and change_fc.field_issue_changes_old_value not in ('a:1:{i:0;a:1:{s:5:"value";s:3:"400";}}')))
 or ( (change_fc.field_issue_changes_field_name = 'field_issue_status'  and change_fc.field_issue_changes_old_value not in ('a:1:{i:0;a:1:{s:5:"value";s:1:"1";}}', 'a:1:{i:0;a:1:{s:5:"value";s:2:"13";}}', 'a:1:{i:0;a:1:{s:5:"value";s:1:"8";}}', 'a:1:{i:0;a:1:{s:5:"value";s:2:"14";}}', 'a:1:{i:0;a:1:{s:5:"value";s:1:"4";}}', 'a:1:{i:0;a:1:{s:5:"value";s:1:"2";}}', 'a:1:{i:0;a:1:{s:5:"value";s:1:"7";}}')))
)

and change_c.changed <= unix_timestamp('2014-12-22 00:00:00')

and change_c.changed > unix_timestamp('2014-12-15 00:00:00')

 and n.created <= unix_timestamp('2014-12-15 00:00:00')

order by n.nid
;

