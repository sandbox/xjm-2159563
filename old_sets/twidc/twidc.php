<?php

define('CSV_DIR', __DIR__ . '/csv');

$dir = scandir(CSV_DIR);
$files = array();
foreach ($dir as $filename) {
  if ($result = strstr($filename, '.txt.out.csv', TRUE)) {
    $files[$result] = file(CSV_DIR . '/' . $filename);
  }
}

foreach ($files as $filename => $file) {
  // Remove quotation marks and whitespace.
  foreach ($file as $i => $line) {
    $files[$filename][$i] = trim(str_replace('"', '', $line));
  }
  // Remove the header row.
  unset($files[$filename][0]);
}

print "

Since the last Drupal Core Updates, we fixed {$files['fixed_majors_criticals'][2]} critical issues and {$files['fixed_majors_criticals'][1]} major issues, and opened {$files['posted_majors_criticals'][2]} criticals and {$files['posted_majors_criticals'][1]} majors. That puts us overall at <a href=\"https://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&status%5B%5D=14&status%5B%5D=4&priorities%5B%5D=400&categories%5B%5D=1&categories%5B%5D=2&version%5B%5D=8.x&issue_tags_op=%3D\">{$files['outstanding_majors_criticals'][2]} release-blocking critical issues</a> and <a href=\"https://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&status%5B%5D=14&status%5B%5D=15&status%5B%5D=4&priorities%5B%5D=300&categories%5B%5D=1&categories%5B%5D=2&version%5B%5D=8.x&issue_tags_op=%3D\">{$files['outstanding_majors_criticals'][1]} major issues</a>.

";
