select distinct(n.nid), n.title, p.field_issue_priority_value, datediff(from_unixtime(change_c.changed), from_unixtime(n.created))
from node n
inner join field_data_field_project project on project.entity_id = n.nid and project.field_project_target_id=3060 -- core
inner join field_data_field_issue_version version on version.entity_id = n.nid and version.field_issue_version_value = '8.0.x-dev'
inner join field_data_field_issue_priority p on p.entity_id = n.nid
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid
inner join field_data_field_issue_changes fc on fc.field_issue_changes_nid = n.nid and fc.field_issue_changes_field_name = 'field_issue_status'
inner join comment change_c on change_c.cid = fc.entity_id
inner join comment octo_c on octo_c.nid = n.nid and octo_c.uid in (65776, 78040, 4166, 99777, 290182)
left join field_data_comment_body octo_cb on octo_c.cid = octo_cb.entity_id
left join field_data_field_issue_changes octo_fc on octo_c.cid = octo_fc.entity_id and fc.field_issue_changes_field_name = 'field_issue_status'
left join field_data_field_issue_files octo_ff on octo_ff.entity_id = n.nid
left join files octo_files on octo_files.fid = octo_ff.field_issue_files_fid
where c.field_issue_category_value in (1, 2) -- bug, task
and s.field_issue_status_value in (2, 7) -- fixed, closed (fixed)
and n.status = 1
and fc.field_issue_changes_new_value = 'a:1:{i:0;a:1:{s:5:"value";s:1:"2";}}'
and change_c.changed > unix_timestamp('2014-10-28 00:00:00')
and (
  char_length(octo_cb.comment_body_value) > 500
  or
  octo_fc.entity_id is not null
  or
  octo_files.filename like '%.patch'
)
order by p.field_issue_priority_value DESC
;
