-- This query returns a best guess at the most recent patch filepath for each
-- NW or NR issue in the core issue queue. It needs to be run against the
-- Drupal.org staging or production database. You should always look at the
-- issue before submitting an updated patch because there is no guarantee that
-- the last patch on the issue is the current, correct patch for the issue.

select ff.entity_id, f.filepath
from files f
inner join field_data_field_issue_files ff on ff.field_issue_files_fid = f.fid
inner join (
  select fd.entity_id as nid, p.field_issue_priority_value as priority, s.field_issue_status_value as status, max(files.timestamp) as max_time
  from files files
  inner join field_data_field_issue_files fd on fd.field_issue_files_fid = files.fid and fd.entity_type = 'node'
  inner join field_data_field_project project on project.entity_id = fd.entity_id and project.field_project_target_id=3060 -- core
  inner join field_data_field_issue_version version on version.entity_id = fd.entity_id and version.field_issue_version_value = '8.0.x-dev' -- 8.x issues
  inner join field_data_field_issue_status s on s.entity_id = fd.entity_id
  inner join field_data_field_issue_priority p on p.entity_id = fd.entity_id
  where files.filepath like '%.patch'
  and s.field_issue_status_value in (8, 14) -- nr, rtbc
  group by fd.entity_id
) max_files on max_files.nid = ff.entity_id and max_files.max_time = f.timestamp
order by max_files.status desc, max_files.priority desc -- RTBCs first, then order by priority
;
