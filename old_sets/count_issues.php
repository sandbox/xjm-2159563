<?php
$files = array();

 const BRANCH = 'D8';
//const BRANCH = 'D7';

// Define months for the start of the cycle and the current month, and the
// priorities to process.
if (BRANCH == 'D8') {
  define('START_DATE', 'March 2011');
  define('END_DATE', date("F Y"));
  $priorities = array('criticals', 'majors');
}
elseif (BRANCH == 'D7') {
  define('START_DATE', 'March 2008');
  define('END_DATE', 'Jan 2011');
  $priorities = array('d7_criticals');
}

// Initialize a nested array of years and months in order.
$ordered_months = array();
foreach (range(2001, date("Y")) as $y) {
  foreach (range(1, 12) as $m) {
    $month_label = date("F Y", strtotime("$y-$m-1"));
    $ordered_months[$month_label] = array();
    // Only go up through the current month.
    if ($month_label == END_DATE) {
      break 2;
    }
  }
}
$month_dates = array_keys($ordered_months);
print_r($ordered_months);

// Load the query results from files.

foreach ($priorities as $priority) {
  $files[$priority]['all'] = file("./$priority/csv/all_issues.txt.out.csv");
  $files[$priority]['fixed'] = file("./$priority/csv/fixed_issues.txt.out.csv");
  $files[$priority]['backport'] = file("./$priority/csv/backports.txt.out.csv");
  $files[$priority]['closed_fixed'] = file("./$priority/csv/revision_workaround_closed_fixed.txt.out.csv");
  $files[$priority]['closed_changed'] = file("./$priority/csv/revision_workaround_node_changed.txt.out.csv");
  if (BRANCH == 'D8') {
    $files[$priority]['count'] = file("./components/csv/top_$priority.txt.out.csv");
  }
}

// Remove header rows from each file.
foreach ($priorities as $priority) {
  // Remove the column labels from each file.
  foreach (array_keys($files[$priority]) as $key) {
    unset($files[$priority][$key][0]);
  }
}

if (BRANCH == 'D8') {
  // Determine the components with the most technical debt.
  // Treat one critical as being worth about 5 majors.
  $debt = array();
  foreach ($files['criticals']['count'] as $row) {
    $row = str_replace('"', '', $row);
    $pieces = explode(',', $row);
    $debt[trim($pieces[0])] = 5 * trim($pieces[1]);
  }
  foreach ($files['majors']['count'] as $row) {
    $row = str_replace('"', '', $row);
    $pieces = explode(',', $row);
    if (empty($debt[trim($pieces[0])])) {
      $debt[trim($pieces[0])] = trim($pieces[1]);
    }
    else {
      $debt[trim($pieces[0])] += trim($pieces[1]);
    }
  }

  // Sort and slice to get the 15 components with the most combined debt.
  arsort($debt);
  $top_debt_components = array_keys(array_slice($debt, 0, 15, TRUE));
}

foreach ($priorities as $priority) {
  $issues = $components = array();
  $by_date = $ordered_months;

  // Add the issues and their posted and fixed dates to the array.
  foreach ($files[$priority]['all'] as $issue) {
    $issue = str_replace('"', '', $issue);
    $pieces = explode(',',$issue);
    $issues[$pieces[0]]['posted'] = strtotime($pieces[1]);
    if (BRANCH == 'D8') {
      $issues[$pieces[0]]['component'] = $components[trim($pieces[2])] = trim($pieces[2]);
    }
    $by_date[date('F Y', $issues[$pieces[0]]['posted'])]['posted'][] = $pieces[0];
  }
  foreach ($files[$priority]['fixed'] as $issue) {
    $issue = str_replace('"', '', $issue);
    $pieces = explode(',',$issue);
    $issues[$pieces[0]]['fixed'] = strtotime($pieces[1]);
    $by_date[date('F Y', $issues[$pieces[0]]['fixed'])]['fixed'][] = $pieces[0];
  }
  foreach ($files[$priority]['backport'] as $issue) {
    $issue = str_replace('"', '', $issue);
    $pieces = explode(',',$issue);
    $issues[$pieces[0]]['posted'] = strtotime($pieces[1]);
    $issues[$pieces[0]]['fixed'] = strtotime($pieces[3]);
    if (BRANCH == 'D8') {
      $issues[$pieces[0]]['component'] = $components[trim($pieces[4])] = trim($pieces[4]);
    }
    $by_date[date('F Y', $issues[$pieces[0]]['posted'])]['posted'][] = $pieces[0];
    $by_date[date('F Y', $issues[$pieces[0]]['fixed'])]['fixed'][] = $pieces[0];
  }

  // Work around https://drupal.org/node/2130059 by adding in approximate fix
  // dates for fixed nodes missing their fixed revision.
  $closed_fixed = 0;
  $closed_changed = 0;
  foreach ($files[$priority]['closed_fixed'] as $issue) {
    $issue = str_replace('"', '', $issue);
    $pieces = explode(',',$issue);
    if (empty($issues[$pieces[0]]['fixed'])) {
      $closed_fixed++;
      $issues[$pieces[0]]['fixed'] = strtotime($pieces[1] . ' -2 weeks');
      $by_date[date('F Y', $issues[$pieces[0]]['fixed'])]['fixed'][] = $pieces[0];
    }
  }
  foreach ($files[$priority]['closed_changed'] as $issue) {
    $issue = str_replace('"', '', $issue);
    $pieces = explode(',',$issue);
    if (empty($issues[$pieces[0]]['fixed'])) {
      $closed_changed++;
      $issues[$pieces[0]]['fixed'] = strtotime($pieces[1] . ' -2 weeks');
      $by_date[date('F Y', $issues[$pieces[0]]['fixed'])]['fixed'][] = $pieces[0];
    }
  }

  print "Used $closed_fixed closed (fixed) revision dates and $closed_changed closed issue changed timestamps for $priority.\n";

  // Write the monthly totals to a CSV file.
  $month_totals = month_totals($by_date);
  $total_csv = array();
  $past_start = FALSE;
  foreach ($month_dates as $month_date) {
    if (!empty($month_totals[$month_date])) {
      // Only add output for the actual release cycle.
      if ($month_date == START_DATE) {
        $past_start = TRUE;
      }
      if ($past_start) {
        // Add the numbers to the CSV output.
        $total_csv[] = $month_date . ",{$month_totals[$month_date]['posted']},{$month_totals[$month_date]['fixed']},{$month_totals[$month_date]['total']}\n";
      }
    }
  }
  file_put_contents("./$priority/overall_monthly_data.csv", $total_csv);

  // Also assemble a list of issues by month and component and the time-to-fix.
  $days_to_fix = array();
  $issue_days_to_fix_csv = '';
  $by_component = array();
  if (BRANCH == 'D8') {
    $components = array_keys($components);
    sort($components);
    foreach ($components as $component) {
      // Initialize the component to an ordered list of months so we can tally
      // over time.
      $by_component[$component] = $ordered_months;
    }
  }
  foreach ($issues as $nid => $issue_data) {
    if (!empty($issue_data['fixed'])) {
      // Estimate how many days the issue was open. I don't care about DST.
      $issue_days_to_fix = ($issue_data['fixed'] - $issue_data['posted']) / 86400;
      $issue_days_to_fix_csv .= $nid . ',' . $issue_days_to_fix . "\n";
    }
    if (!empty($issue_data['component'])) {
      $by_component[$issue_data['component']][date('F Y', $issue_data['posted'])]['posted'][] = $nid;
      if (!empty($issue_data['fixed'])) {
        $by_component[$issue_data['component']][date('F Y', $issue_data['fixed'])]['fixed'][] = $nid;
        $days_to_fix[$issue_data['component']][] = $issue_days_to_fix;
      }
    }
  }

  // Output CSV for posted, fixed, and total component issues.
  if (BRANCH == 'D8') {
    // Prepare component total and days-to-fix data.
    $component_totals = array();
    $avg_days_to_fix = array();
    foreach ($by_component as $component => $component_data) {
      // Tally monthly totals for the outstanding issues in the component.
      $component_totals[$component] = month_totals($component_data);
      if (!empty($days_to_fix[$component])) {
        // Find the average time-to-fix for fixed issues in the component.
        $avg_days_to_fix[$component] = ceil(array_sum($days_to_fix[$component]) / sizeof($days_to_fix[$component]));
      }
    }

    foreach (array('posted', 'fixed', 'total') as $count_type) {
      $component_csv = array();

      // Add a header row with the component names.
      $header = 'date';
      foreach (array_keys($component_totals) as $component) {
        if (!empty($component_totals[$component][END_DATE]['total']) && (in_array($component, $top_debt_components))) {
          $header .= ",$component";
        }
      }

      $past_start = FALSE;
      foreach ($month_dates as $month_date) {
        // Only add output for the actual release cycle.
        if ($month_date == START_DATE) {
          $past_start = TRUE;
        }
        if ($past_start) {
          $month_row = $month_date;
          foreach ($component_totals as $component => $component_counts) {
            if (!empty($component_totals[$component][END_DATE]['total']) && (in_array($component, $top_debt_components))) {
              $count = empty($component_counts[$month_date][$count_type]) ? 0 : $component_counts[$month_date][$count_type];
              $month_row .= "," . $count;
            }
          }
          $component_csv[] = $month_row;
        }
      }
      array_unshift($component_csv, $header);

      file_put_contents("./$priority/{$count_type}_component_data.csv", implode("\n", $component_csv) . "\n");
    }

    // Output CSV for average time-to-fix per component, plus raw time to fix.
    $days_to_fix_csv = "component,average days to fix\n";
    foreach ($avg_days_to_fix as $component => $days) {
      if (!empty($component_totals[$component][END_DATE]['total']) && (in_array($component, $top_debt_components))) {
        $days_to_fix_csv .= "$component,$days\n";
      }
    }
    file_put_contents("./$priority/component_days_to_fix.csv", $days_to_fix_csv);
  }
  file_put_contents("./$priority/issue_days_to_fix.csv", $issue_days_to_fix_csv);
}

function month_totals($by_date) {
  // Track running totals.
  $total_posted = $total_fixed = 0;
  $month_totals = array();
  $any_issues = FALSE;
  foreach ($by_date as $month_date => $month_issues) {
    // Indicate the number posted and fixed that month.
    $posted = empty($month_issues['posted']) ? 0 : sizeof($month_issues['posted']);
    $fixed = empty($month_issues['fixed']) ? 0 : sizeof($month_issues['fixed']);
    $month_totals[$month_date]['posted'] = $posted;
    $month_totals[$month_date]['fixed'] = $fixed;

    $total_posted += $month_totals[$month_date]['posted'];
    $total_fixed += $month_totals[$month_date]['fixed'];

    // Infer the total oustanding criticals that month by subtracting.
    $month_totals[$month_date]['total'] = $total_posted - $total_fixed;
  }
  return $month_totals;
}
