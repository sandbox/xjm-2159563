select issues.component, count(issues.nid) as open_issues, datediff(now(), from_unixtime(avg(issues.recent_comment))) as average_age
from (
  select n.nid, cmp.field_issue_component_value component, max(comment.changed) as recent_comment
  from node n
  inner join field_data_field_project project on project.entity_id = n.nid and project.field_project_target_id=3060 -- core
  inner join field_data_field_issue_version version on version.entity_id = n.nid and version.field_issue_version_value = '8.0.x-dev'
  inner join field_data_field_issue_priority p on p.entity_id = n.nid
  inner join field_data_field_issue_category c on c.entity_id = n.nid
  inner join field_data_field_issue_status s on s.entity_id = n.nid
  inner join field_data_field_issue_component cmp on cmp.entity_id = n.nid
  inner join comment on comment.nid = n.nid
  where p.field_issue_priority_value = 300 -- major
  and c.field_issue_category_value in (1, 2) -- bug, task
  and s.field_issue_status_value in (1, 13, 8, 14, 4) -- active, nw, nr, rtbc, postponed
  and n.status = 1
  group by comment.nid
) issues
group by issues.component
order by issues.component
;
