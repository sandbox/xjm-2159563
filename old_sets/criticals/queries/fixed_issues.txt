select nr.nid, date(from_unixtime(max(nr.timestamp))) as fixed_date -- only the most recent fixed revision
from node_revision nr
inner join
( -- use a subquery to ensure we have no false positives from unusual issue workflows
select n.nid, date(from_unixtime(n.created)) as posted_date
from node n
inner join field_data_field_project project on (project.entity_id = n.nid and project.field_project_target_id=3060) -- core
inner join field_data_field_issue_version version on (version.entity_id = n.nid and version.field_issue_version_value = '8.0.x-dev') -- 8.x issues
inner join field_data_field_issue_priority p on p.entity_id = n.nid
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid
where p.field_issue_priority_value = 400 -- critical
and c.field_issue_category_value in (1, 2) -- bug, task
and s.field_issue_status_value in (2, 7) -- fixed, closed (fixed)
and n.status = 1
order by n.created
) fixed_issues on nr.nid = fixed_issues.nid
inner join field_revision_field_issue_status rs on rs.entity_id = nr.nid and rs.revision_id = nr.vid and rs.field_issue_status_value = 2 -- fixed
group by nr.nid
;

