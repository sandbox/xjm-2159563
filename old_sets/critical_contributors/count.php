<?php

define('CSV_DIR', __DIR__ . '/csv');
define('WRITE_DIR', __DIR__);

$dir = scandir(CSV_DIR);
$files = array();
foreach ($dir as $filename) {
  if ($result = strstr($filename, '.txt.out.csv', TRUE)) {
    $files[$result] = file(CSV_DIR . '/' . $filename);
  }
}

$data = array();

foreach ($files as $filename => $file) {
  // Remove the header row.
  unset($files[$filename][0]);
  // Remove quotation marks and whitespace.
  foreach ($file as $i => $line) {
    $files[$filename][$i] = trim(str_replace('"', '', $line));
    // Parse out the key/value pairs.
    $data[$filename][$i] = explode(',', $files[$filename][$i]);
  }
}

$issue_contributors = array();
$issue_companies = array();
foreach ($data as $filename => $file_data) {
  foreach ($file_data as $record) {
    switch ($record[2]) {
      case 'NULL':
      case 'freelancer':
      case 'Freelance':
        $company_name = '(independent)';
        break;

      case 'Tag1 consulting':
        $company_name = 'Tag1 Consulting';
        break;

      default:
        $company_name = $record[2];
        break;
    }

    $issue_contributors[$record[1]][$record[0]] = TRUE;
    $issue_companies[$company_name][$record[0]] = TRUE;
  }
}


/*
$contributors = array();
foreach ($issue_contributors as $contributor => $issues) {
  $contributors[$contributor] = sizeof($issues);
}

arsort($contributors);
foreach ($contributors as $contributor => $count) {
  print $contributor . ',' . $count . "\n";
}

print "









";
*/
$companies = array();
foreach ($issue_companies as $company => $issues) {
  $companies[$company] = sizeof($issues);
}

arsort($companies);
foreach ($companies as $company => $count) {
  print $company . ',' . $count . "\n";
}
