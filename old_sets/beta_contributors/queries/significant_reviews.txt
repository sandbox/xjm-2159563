select n.nid, u.name, org.field_current_company_org_value
from node n
inner join field_data_field_project project on project.entity_id = n.nid and project.field_project_target_id=3060 -- core
inner join field_data_field_issue_version version on version.entity_id = n.nid and version.field_issue_version_value = '8.0.x-dev'
inner join field_data_field_issue_status s on s.entity_id = n.nid and (s.field_issue_status_value = 2 OR s.field_issue_status_value = 7)
inner join taxonomy_index ti on ti.nid = n.nid and ti.tid = 8912 -- beta blocker
inner join comment cmt on cmt.nid = n.nid
inner join users u on u.uid = cmt.uid
left join field_data_field_current_company_org org on org.entity_id = u.uid
inner join field_data_comment_body cb on cmt.cid = cb.entity_id
where u.uid <> 180064
and char_length(cb.comment_body_value) > 500
and cmt.created > unix_timestamp('2013-12-06 00:00:00')
group by n.nid, u.name
;
