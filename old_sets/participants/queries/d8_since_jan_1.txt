select count(distinct(c.uid))
from node n
inner join comment c on c.nid = n.nid
inner join field_data_field_project project on (project.entity_id = n.nid and project.field_project_target_id=3060) -- core
inner join field_data_field_issue_category cat on cat.entity_id = n.nid
inner join field_data_field_issue_version version on (version.entity_id = n.nid and version.field_issue_version_value = '8.0.x-dev') -- 8.x issues
where cat.field_issue_category_value in (1, 2, 3) -- bug, task, feature request
and n.status = 1
and c.created > unix_timestamp('2014-01-01')
order by n.created
;
