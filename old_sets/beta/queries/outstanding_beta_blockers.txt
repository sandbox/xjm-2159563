select cmp.field_issue_component_value component, count(n.nid) as outstanding_beta_blockers
from node n
inner join field_data_field_project project on (project.entity_id = n.nid and project.field_project_target_id=3060) -- core
inner join field_data_field_issue_version version on (version.entity_id = n.nid and version.field_issue_version_value = '8.0.x-dev')
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid
inner join field_data_field_issue_component cmp on cmp.entity_id = n.nid
inner join taxonomy_index ti on ti.nid = n.nid and ti.tid = 8912 -- beta blocker
where s.field_issue_status_value in (1, 13, 8, 14, 4) -- active, nw, nr, rtbc, postponed
and n.status = 1
group by component
order by outstanding_beta_blockers desc
;
