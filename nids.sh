echo -e "Enter the node IDs from last time's worksheet, followed by q to quit:"
read -d q old
echo -e "\nEnter the node IDs from the latest CSV file, followed by q to quit:"
read -d q new

echo -e "\n"

nids=`php ./nids.php "$old" "$new"`

if [[ ! -z $nids ]] ; then
  php ./nids.php "$old" "$new" | pbcopy
  echo -e "\n*** The following new node IDs have been copied to the clipboard: ***\n"
  echo -e $nids
else
  echo -e "\n*** No new node IDs to add. ***\n"
fi
