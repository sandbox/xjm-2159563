<?php

/**
 * @file
 * Add Markdown for items not already listed in a given release notes file.
 */

include_once(__DIR__ . '/../src/ResultSet.php');

use Drupal\core_metrics\ResultSet;

$r = new ResultSet('core_release');

$criticals = $r->getResult('fixed_criticals');
$fixed_rn_mention = $r->getResult('fixed_rn_mention');
$open_rn_mention = $r->getResult('open_rn_mention');

if (empty($argv[1]) || !file_exists($argv[1])) {
  die("Usage: php minor_release_md.php /path/to/patched/CHANGELOG.txt\n");
}
$rn_so_far = file_get_contents($argv[1]);

if ($unlisted_fixed = unlisted_issues($fixed_rn_mention, $rn_so_far)) {
  print "## Unlisted fixed issues\n\n";
  print "(Move these bullets into the appropriate sections above and draft more descriptive text for them if appropriate.)\n\n";
  print $unlisted_fixed . "\n\n";
}

if ($unlisted_criticals = unlisted_issues($criticals, $rn_so_far)) {
  print "## Unlisted criticals\n\n";
  print "(These issues are fixed critical issues for this branch. Check each to ensure that it is not fixed in the previous branch. (If it is, update its branch field to be the lowest branch it was backported to.) Move these bullets into the appropriate sections above and draft more descriptive text for them if appropriate.)\n\n";
  print $unlisted_criticals . "\n\n";
}

if ($unlisted_open = unlisted_issues($open_rn_mention, $rn_so_far)) {
  print "## Unlisted open issues\n\n";
  print "(These issues are tagged for the release notes, but still open. Some issues might be tagged to highlight a certain outstanding bug in the release notes. Others might have been reopened for a backport or followup. Some might have been tagged prematurely or incorrectly. Check the issue carefully, and if appropriate, move these bullets into the appropriate sections above and draft more descriptive text. Check with a release manager if you are unsure.)\n\n";
  print $unlisted_open . "\n\n";
}

function unlisted_issues($set, $rn_so_far) {
  $output = '';
  foreach ($set as $issue) {
    $nid = $issue[0];
    $title = $issue[1];
    if (strpos($rn_so_far, $nid) === FALSE) {
      $output .= "* [$title](https://www.drupal.org/node/$nid)\n";
    }
  }
  return $output;
}