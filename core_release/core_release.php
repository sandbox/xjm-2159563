<?php

/**
 * @file
 * Convert query results into HTML for the release notes.
 */

include_once(__DIR__ . '/../src/ResultSet.php');

use Drupal\core_metrics\ResultSet;

$r = new ResultSet('core_release');

$criticals = $r->getResult('fixed_criticals');
$fixed_rn_mention = $r->getResult('fixed_rn_mention');
$open_rn_mention = $r->getResult('open_rn_mention');

if (!empty($fixed_rn_mention)) {
  print "<h3>Important update information</h3>\n\n<ul>\n";

  foreach ($fixed_rn_mention as $issue) {
    $nid = $issue[0];
    $title = $issue[1];
    print "<li><a href=\"https://www.drupal.org/node/$nid\">#$nid: $title</a></li>\n";
  }

  print "</ul>\n\n";
}


if (!empty($criticals)) {
  print "<h3>Important bug fixes</h3>\n\n<ul>\n";

  foreach ($criticals as $issue) {
    $nid = $issue[0];
    $title = $issue[1];
    print "<li><a href=\"https://www.drupal.org/node/$nid\">#$nid: $title</a></li>\n";
  }

  print "</ul>\n\n";
}
