<?php

include_once(__DIR__ . '/../src/ResultSet.php');

use Drupal\core_metrics\ResultSet;

$r = new ResultSet('critical_data');
$column = $r->getColumn('critical_data', 'nid');
$old_nids = file(__DIR__ . '/nids.txt', FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);

$missing = array_diff($column, $old_nids);

foreach ($missing as $nid) {
  print $nid . "\n";
}