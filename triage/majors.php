<?php

include_once(__DIR__ . '/../src/ResultSet.php');

use Drupal\core_metrics\ResultSet;

$r = new ResultSet('triage');

$column = $r->getColumn('untriaged_major_bugs', 'component');
$per_component = array_count_values($column);
arsort($per_component);
print "<table><caption>Updated: " . date('Y-m-d') . "</caption>\n";
print "<tr><th>Component</th><th>Count</th><th>Untriaged bug queue</th><tr>\n";
foreach ($per_component as $component => $count) {
  $link = "https://www.drupal.org/project/issues/search/drupal?project_issue_followers=&status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&status%5B%5D=14&status%5B%5D=4&priorities%5B%5D=300&categories%5B%5D=1&version%5B%5D=8.x&component%5B%5D=$component&issue_tags_op=%3C%3E&issue_tags=Triaged+D8+major%2C+Triaged+for+D8+major+current+state%2C+D8+major+triage+deferred";
  print "<tr><td>$component</td><td>$count</td><td><a href=\"$link\">$component bugs</a></td></tr>\n";
}
print "</table>\n";