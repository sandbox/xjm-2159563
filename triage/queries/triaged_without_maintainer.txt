

select distinct n.nid,
replace(n.title, '"', '') as title
from node n

inner join field_data_field_project project on project.entity_id = n.nid
inner join field_data_field_issue_version version on version.entity_id = n.nid
inner join field_data_field_issue_priority p on p.entity_id = n.nid
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid


inner join taxonomy_index triaged_critical_ti on n.nid = triaged_critical_ti.nid and triaged_critical_ti.tid = '164349'

inner join field_data_field_issue_changes triaged_fc on triaged_fc.field_issue_changes_nid = n.nid
inner join comment triaged_c on triaged_c.cid = triaged_fc.entity_id


where n.status = 1
 and (project.field_project_target_id  in ('3060'))
 and (version.field_issue_version_value  in ('8.3.x-dev', '8.4.x-dev', '8.5.x-dev'))
 and (p.field_issue_priority_value  in ('400'))
 and (c.field_issue_category_value  in ('1', '2', '5', '3'))
 and (s.field_issue_status_value  in ('1', '13', '8', '14', '4'))

 and ( (triaged_fc.field_issue_changes_field_name = 'taxonomy_vocabulary_9'  and  (triaged_fc.field_issue_changes_new_value  like '%Triaged D8 critical%') ))
  and ( (triaged_fc.field_issue_changes_field_name = 'taxonomy_vocabulary_9'  and  (triaged_fc.field_issue_changes_old_value not like '%Triaged D8 critical%') ))
  and (triaged_c.uid not in ('157725', '78040', '35733', '1167326', '395439', '1078742', '183211', '65776', '1850070', '1', '24967', '41502', '4166'))


order by n.nid
;

