

select distinct n.nid,
replace(n.title, '"', '') as title,
datediff(now(), from_unixtime(n.created)) as age,
datediff(now(), from_unixtime(node_comments.recent_comment_date)) as last_update,
datediff(now(), from_unixtime(node_files.last_patch_date)) as patch_updated
from node n

inner join field_data_field_project project on project.entity_id = n.nid
inner join field_data_field_issue_version version on version.entity_id = n.nid
inner join field_data_field_issue_priority p on p.entity_id = n.nid
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid


inner join field_data_field_issue_component cmp on cmp.entity_id = n.nid

left join (
select n.nid, count(cm.cid) as comment_count, max(cm.changed) as recent_comment_date, replace(group_concat(distinct(u.name)), ',', ', ') as commenters 

from node n

inner join field_data_field_project project on project.entity_id = n.nid
inner join field_data_field_issue_version version on version.entity_id = n.nid
inner join field_data_field_issue_priority p on p.entity_id = n.nid
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid
inner join comment cm on cm.nid = n.nid 
inner join users u on u.uid = cm.uid 
 and (project.field_project_target_id  in ('3060'))
 and (version.field_issue_version_value  in ('8.3.x-dev', '8.4.x-dev', '8.5.x-dev'))
 and (p.field_issue_priority_value  in ('300'))
 and (c.field_issue_category_value  in ('1'))
 and (s.field_issue_status_value  in ('1', '13', '8', '14', '4'))
group by n.nid 
) node_comments on node_comments.nid = n.nid
left join (
select n.nid, max(files.timestamp) as last_patch_date
from node n

inner join field_data_field_project project on project.entity_id = n.nid
inner join field_data_field_issue_version version on version.entity_id = n.nid
inner join field_data_field_issue_priority p on p.entity_id = n.nid
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid
inner join field_data_field_issue_files i_files on i_files.entity_id = n.nid 
inner join file_managed files on i_files.field_issue_files_fid = files.fid 
 and (project.field_project_target_id  in ('3060'))
 and (version.field_issue_version_value  in ('8.3.x-dev', '8.4.x-dev', '8.5.x-dev'))
 and (p.field_issue_priority_value  in ('300'))
 and (c.field_issue_category_value  in ('1'))
 and (s.field_issue_status_value  in ('1', '13', '8', '14', '4'))
and files.filename LIKE '%.patch' 
group by n.nid 
) node_files on node_files.nid = n.nid
where n.status = 1
 and (project.field_project_target_id  in ('3060'))
 and (version.field_issue_version_value  in ('8.3.x-dev', '8.4.x-dev', '8.5.x-dev'))
 and (p.field_issue_priority_value  in ('300'))
 and (c.field_issue_category_value  in ('1'))
 and (s.field_issue_status_value  in ('1', '13', '8', '14', '4'))

 and n.nid not in (select n.nid 
from node n

inner join field_data_field_project project on project.entity_id = n.nid
inner join field_data_field_issue_version version on version.entity_id = n.nid
inner join field_data_field_issue_priority p on p.entity_id = n.nid
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid
inner join taxonomy_index term_ti on n.nid = term_ti.nid and term_ti.tid = '174642'
 and (project.field_project_target_id  in ('3060'))
 and (version.field_issue_version_value  in ('8.3.x-dev', '8.4.x-dev', '8.5.x-dev'))
 and (p.field_issue_priority_value  in ('300'))
 and (c.field_issue_category_value  in ('1'))
 and (s.field_issue_status_value  in ('1', '13', '8', '14', '4'))
)
 and n.nid not in (select n.nid 
from node n

inner join field_data_field_project project on project.entity_id = n.nid
inner join field_data_field_issue_version version on version.entity_id = n.nid
inner join field_data_field_issue_priority p on p.entity_id = n.nid
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid
inner join taxonomy_index term_ti on n.nid = term_ti.nid and term_ti.tid = '177412'
 and (project.field_project_target_id  in ('3060'))
 and (version.field_issue_version_value  in ('8.3.x-dev', '8.4.x-dev', '8.5.x-dev'))
 and (p.field_issue_priority_value  in ('300'))
 and (c.field_issue_category_value  in ('1'))
 and (s.field_issue_status_value  in ('1', '13', '8', '14', '4'))
)
 and (cmp.field_issue_component_value in ('base system'))


order by last_patch_date, n.nid
;

