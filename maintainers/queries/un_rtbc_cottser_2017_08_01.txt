

select count(distinct n.nid) as node_count
from node n

inner join field_data_field_project project on project.entity_id = n.nid
inner join field_data_field_issue_version version on version.entity_id = n.nid
inner join field_data_field_issue_priority p on p.entity_id = n.nid
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid


inner join field_data_field_issue_changes changed_fc on changed_fc.field_issue_changes_nid = n.nid
inner join comment changed_c on changed_c.cid = changed_fc.entity_id


where n.status = 1
 and (project.field_project_target_id  in ('3060'))
 and (version.field_issue_version_value  in ('8.3.x-dev', '8.4.x-dev', '8.5.x-dev'))
 and (c.field_issue_category_value  in ('1', '2', '5', '3'))
 and (s.field_issue_status_value  in ('1', '13', '8', '14', '4', '2', '7'))

 and ( (changed_fc.field_issue_changes_field_name = 'field_issue_status'  and changed_fc.field_issue_changes_new_value  in ('a:1:{i:0;a:1:{s:5:"value";s:2:"13";}}', 'a:1:{i:0;a:1:{s:5:"value";s:1:"8";}}')))
  and ( (changed_fc.field_issue_changes_field_name = 'field_issue_status'  and changed_fc.field_issue_changes_old_value  in ('a:1:{i:0;a:1:{s:5:"value";s:2:"14";}}')))
  and (changed_c.uid  in ('1167326'))


and changed_c.changed > unix_timestamp('2017-08-01 00:00:00')

and changed_c.changed <= unix_timestamp('2017-09-01 00:00:00')

order by n.nid
;

