

select distinct n.nid
from node n

inner join field_data_field_project project on project.entity_id = n.nid
inner join field_data_field_issue_version version on version.entity_id = n.nid
inner join field_data_field_issue_priority p on p.entity_id = n.nid
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid


inner join taxonomy_index fm_review_ti on n.nid = fm_review_ti.nid and fm_review_ti.tid = '169963'

inner join comment commenter_c on commenter_c.nid = n.nid and commenter_c.uid in (78040)

where n.status = 1
 and (project.field_project_target_id  in ('3060'))
 and (version.field_issue_version_value  in ('8.2.x-dev', '8.3.x-dev', '8.4.x-dev'))
 and (c.field_issue_category_value  in ('1', '2', '5', '3'))
 and (s.field_issue_status_value  in ('1', '13', '8', '14', '4', '2', '7'))

 and commenter_c.changed <= unix_timestamp('2017-03-02 00:00:00') and commenter_c.changed > unix_timestamp('2017-01-01 00:00:00')

order by n.nid
;

