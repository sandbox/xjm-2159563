<?php

// Get the name of the query set from the script.
if (empty($argv[1])) {
  exit("Usage: php build_queries.php query_set_name\n(where query_set_name is a subdirectory containing a query set)\n");
}

$set = $argv[1];

include_once(__DIR__ . '/src/IssueQuery.php');
include_once(__DIR__ . '/src/QueryBuilderBase.php');
include_once(__DIR__ . '/src/' . $set . '/QueryBuilder.php');

$builder_name = '\\Drupal\\core_metrics\\' . $set . '\\QueryBuilder';
$builder = new $builder_name;
foreach ($builder->getQueries() as $key => $q) {
  file_put_contents(__DIR__ . "/$set/queries/$key.txt", $q->buildQuery());
}
