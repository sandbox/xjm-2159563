#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
FILES="$DIR/"$1"/queries/*.txt"
for f in $FILES
do
  if [ -e /var/www/staging.devdrupal.org/altdb ]; then db=drupalorg_staging1; else db=drupalorg_staging; fi; mysql -u xjm -p$(cat /home/xjm/db.info) -h stagingdb1.drupal.aws $db < "$f" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > "$f".out.csv
done
mv "$DIR"/"$1"/queries/*.csv "$DIR"/"$1"/csv/

