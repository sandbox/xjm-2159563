<?php

$old = [
];

$new = [
];

if (!empty($argv[1])) {
  $old = explode("\n", $argv[1]);
}
if (!empty($argv[2])) {
  $new = explode("\n", $argv[2]);
}

$diff = array_diff($new, $old);

foreach ($diff as $nid) {
  print $nid . "\n";
}
