select distinct n.nid, n.title
from node n
inner join field_data_field_project project on project.entity_id = n.nid
inner join field_data_field_issue_version version on version.entity_id = n.nid
inner join field_data_field_issue_priority p on p.entity_id = n.nid
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid
inner join field_data_field_issue_changes fc on fc.field_issue_changes_nid = n.nid
inner join comment change_c on change_c.cid = fc.entity_id
where n.status = 1
and
(
  (fc.field_issue_changes_field_name = 'field_project' and fc.field_issue_changes_old_value = 'a:1:{i:0;a:1:{s:9:"target_id";s:4:"3060";}}')
   or project.field_project_target_id=3060
) -- was or is in the core queue
and (
  (fc.field_issue_changes_field_name = 'field_issue_version' and fc.field_issue_changes_old_value = 'a:1:{i:0;a:1:{s:5:"value";s:9:"8.0.x-dev";}}')
  or version.field_issue_version_value = '8.0.x-dev'
) -- was or is is 8.0.x-dev
and (
  (fc.field_issue_changes_field_name = 'field_issue_status' and fc.field_issue_changes_old_value in (
    'a:1:{i:0;a:1:{s:5:"value";s:1:"1";}}',
    'a:1:{i:0;a:1:{s:5:"value";s:2:"13";}}',
    'a:1:{i:0;a:1:{s:5:"value";s:1:"8";}}',
    'a:1:{i:0;a:1:{s:5:"value";s:2:"14";}}',
    'a:1:{i:0;a:1:{s:5:"value";s:1:"4";}}'
  ))
  or s.field_issue_status_value in (1, 13, 8, 14, 4)
) -- was or is one of active, nw, nr, rtbc, postponed
and (
  (fc.field_issue_changes_field_name = 'field_issue_category' and fc.field_issue_changes_old_value not in (
    'a:1:{i:0;a:1:{s:5:"value";s:3:"bug";}}',
    'a:1:{i:0;a:1:{s:5:"value";s:4:"task";}}',
    'a:1:{i:0;a:1:{s:5:"value";s:4:"plan";}}'
  ))
  or c.field_issue_category_value in (1, 2, 5)
) -- was or is a bug, task, or plan
and (
  (fc.field_issue_changes_field_name = 'field_issue_priority' and fc.field_issue_changes_old_value = 'a:1:{i:0;a:1:{s:5:"value";s:3:"400";}}')
  or p.field_issue_priority_value = 400 -- critical
) -- was or is critical
and n.nid not in (
  select nid from node n
  inner join field_data_field_project project on project.entity_id = n.nid and project.field_project_target_id=3060 -- core
  inner join field_data_field_issue_version version on version.entity_id = n.nid and version.field_issue_version_value = '8.0.x-dev'
  inner join field_data_field_issue_priority p on p.entity_id = n.nid and p.field_issue_priority_value = 400 -- critical
  inner join field_data_field_issue_category c on c.entity_id = n.nid
  inner join field_data_field_issue_status s on s.entity_id = n.nid
  where c.field_issue_category_value in (1, 2, 5) -- bug, task, plan
  and s.field_issue_status_value in (1, 13, 8, 14, 4, 2, 7) -- active, nw, nr, rtbc, postponed, fixed, closed (fixed)
  and n.status = 1
) -- Not a current or fixed critical
and change_c.changed > unix_timestamp(date_sub((select from_unixtime(changed) from node order by changed desc limit 1), interval 7 day))
and n.created <= unix_timestamp(date_sub((select from_unixtime(changed) from node order by changed desc limit 1), interval 7 day))
;
