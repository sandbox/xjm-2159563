

select count(distinct n.nid) as node_count,
cmp.field_issue_component_value as component
from node n

inner join field_data_field_project project on project.entity_id = n.nid
inner join field_data_field_issue_version version on version.entity_id = n.nid
inner join field_data_field_issue_priority p on p.entity_id = n.nid
inner join field_data_field_issue_category c on c.entity_id = n.nid
inner join field_data_field_issue_status s on s.entity_id = n.nid


inner join field_data_field_issue_component cmp on cmp.entity_id = n.nid

where n.status = 1
 and (project.field_project_target_id  in ('3060'))
 and (version.field_issue_version_value  in ('8.1.x-dev', '8.2.x-dev', '8.3.x-dev'))
 and (p.field_issue_priority_value  in ('300'))
 and (c.field_issue_category_value  in ('1'))
 and (s.field_issue_status_value  in ('1', '13', '8', '14', '4'))



group by component
order by node_count
;

